Code to process all-day IMU data from .h5 files and produce some plots of stride length, stride speed, and walking bouts. 


## Usage 

In main.jl under the function declarations find the lines to set subject number, day number and path to data. The code is set up to look for .h5 files inside folders labelled with subject number and day like this: /path/to/files/subject1/day1/*.h5. 

When running main.jl, the walking filter requires a threshold to be set for the distance function. Points below this threshold can be considered walking. If the distance function is not displayed, it will be saved as a png in the local directory. Look at the distance function and choose some threshold where some points on the function are below but not all. You can also set different thresholds for different sections of data. When prompted, enter the x-values of the plot where the threshold changes, or enter 0 is only one threshold is needed. Then enter the threshold(s). See dtw.pdf for more information on the walking filter. 

Plots will be saved in the same base directory as the data.

## make_plots.jl

Make sure path to data is correct before running make_plots. Set subject numbers and days according to which results you want to see. Gait parameters from these subjects and days are put into the arrays frwd_all, speed_all, steps_all, etc.

Data can be found here: smb://AcademicFS.uc.ucalgary.ca/KuoGroup