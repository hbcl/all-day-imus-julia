using Plots
using Parameters
using HDF5
using FFTW
using FindPeaks1D
using Statistics
using Optim
using Images, ImageView
using FindPeaks1D
using Glob
using DelimitedFiles
using FileIO, JLD2


mutable struct IMUData
    W::Array{Float64,2}
    A::Array{Float64,2}
    M::Array{Float64,2}
    t::Array{UInt64,1}
    PERIOD::Float64
    Wm::Array{Float64,1}
    Am::Array{Float64,1}
end

mutable struct WalkData
    FFstart::Array{Int64,1}
    FFend::Array{Int64,1}
    walkSection::Array{Bool,1}
    Anz::Array{Float64,2}
    An::Array{Float64,2}
    A::Array{Float64,2}
    W::Array{Float64,2}
    V::Array{Float64,2}
    Vm::Array{Float64,1}
    P::Array{Float64,2}
    quaternion::Array{Float64,2}
    PERIOD::Float64
end

mutable struct StrideData
	frwdSwing::Array{Float64,2}
	ltrlSwing::Array{Float64,2}
	frwd::Array{Float64,2}  
	ltrl::Array{Float64,2}
	absLtrl::Array{Float64,2}
	absFrwd::Array{Float64,2}
	elev::Array{Float64,2}
    theta::Array{Float64,2}
	startEnd::Array{Float64,2}
	footHeading::Array{Float64,1}
	diffFootHeading::Array{Float64,1}
	stepSamples::Array{Float64,1} 
	time::Array{Float64,1} 
	frwdSpeed::Array{Float64,1} 
    length::Array{Float64,1}
end

mutable struct DTWvar
    tw1::Array{Float64,1}
    tw2::Array{Float64,1}
    tw3::Array{Float64,1}
    ta1::Array{Float64,1}
    ta2::Array{Float64,1}
    ta3::Array{Float64,1}
    startpoint::Array{UInt64,1}
    endpoint::Array{UInt64,1}
    distanceFunc::Array{Float64,1}
    walkSection::Array{Bool,1}
    threshinds::Array{Int,1}
    threshs::Array{Float64,1}
end

@with_kw struct config # all the useful constants and their default values
    gravity = 9.8
    useKF = true # whether or not to use kalman filter
    useWalkFilter = true # whether or not to use walking filter
    wFF = 60*pi/180 # threshold w has to be below for footfall, in radians
    aFF = 2 # threshold a has to be below for footfall
    tFF = 0.4 # minimum time between footfalls, in seconds
    maxTFF = 0.8 # Maximum rest period allowed before splitting into 2 FFs
    dirSteps = 3 # how many steps before and after current step are used to set direction
    maxWalkDif = 4 # maximum rest period between steps before splitting into 2 walking sections
end

function set_orientation(imu::IMUData,orientation)
    # orientation options
    ORIGINAL = 0;
	LED_UP_RIGHT_FRWD = 1; # This one is normally used on feet
	LED_UP_LEFT_FRWD = 2;
	LED_LOW_RIGHT_FRWD = 3;
	LED_LOW_DOWN_FRWD = 4;
	LED_UP_RIGHT_BACK = 5; 
	LED_LOW_LEFT_BACK = 6;

    wx = imu.W[:,1]*imu.PERIOD
    wy = imu.W[:,2]*imu.PERIOD
    wz = imu.W[:,3]*imu.PERIOD
    ax = imu.A[:,1]
    ay = imu.A[:,2]
    az = imu.A[:,3]

    if orientation == ORIGINAL
        imu.W = [wx wy wz]
        imu.A = [ax ay az]
    elseif orientation == LED_UP_RIGHT_FRWD
        imu.W = [wy wx -wz]
        imu.A = [ay ax -az]
    elseif orientation == LED_UP_LEFT_FRWD
        imu.W = [wx -wy -wz]
        imu.A = [ax -ay -az]
    elseif orientation == LED_LOW_RIGHT_FRWD
        imu.W = [-wx wy -wz]
        imu.A = [-ax ay -az]
    elseif orientation == LED_LOW_DOWN_FRWD
        imu.W = [-wy -wx -wz]
        imu.A = [-ay -ax -az]
    elseif orientation == LED_UP_RIGHT_BACK
        imu.W = [wz -wx wy]
        imu.A = [az -ax ay]
    elseif orientation == LED_LOW_LEFT_BACK
        imu.W = [wz wx -wy]
        imu.A = [az ax -ay]
    end

end

function sync_apdm_combined(filename,IMUlabels)
# use this function to extract data from IMUs
# if they're all together in one file
numIMUs = length(IMUlabels)
notSet = ones(Bool, numIMUs)
IMUarray = Array{IMUData, 1}(undef, numIMUs)
fid = h5open(filename, "r")
g = fid["Sensors"]
for obj in g
    a = read(obj,"Accelerometer")
    w = read(obj,"Gyroscope")
    m = read(obj,"Magnetometer")
    t = read(obj,"Time")
    # check the label for this IMU
    congroup = open_group(obj,"Configuration")
    attr = open_attribute(congroup,"Sample Rate")
    freq = read(attr)
    period = 1/freq
    attr = open_attribute(congroup,"Label 0")
    label = read(attr)
    ind = findfirst(k->occursin(k,label),IMUlabels)
    if ind===nothing
        println("Not using sensor ", label)
    else
        IMUarray[ind] = IMUData(transpose(w),transpose(a),transpose(m),t,period,vec(sum(w.^2,dims=1).^0.5),vec(sum(a.^2,dims=1).^0.5))
        getdata(IMUarray[ind],[1 size(w,2)])
        notSet[ind] = 0
        set_orientation(IMUarray[ind],1)
    end

end
if length(findall(notSet)) > 0
    for ind in findall(notSet)
        println("Could not find sensor ", IMUlabels[ind])
    end
    actualNumIMUs = numIMUs - length(findall(notSet))
    IMUarraynew = Array{IMUData, 1}(undef, actualNumIMUs)
    inds = findall(x -> x == 0, notSet)
    for i=1:actualNumIMUs
        IMUarraynew[i] = IMUarray[inds[i]]
    end
    IMUarray = IMUarraynew
end
close(fid)
return IMUarray

end


function sync_apdm_separate(filenames)
# use this function to extract data from IMUs
# if each IMU has a separate file
# filenames is an array of strings
# returns IMUData array in same order as filenames
nfiles = length(filenames)
IMUarray = Array{IMUData, 1}(undef, nfiles)
for ifile=1:nfiles
    fid = h5open(filenames[ifile], "r")
    g = fid["Sensors"]
    for obj in g # should be only one
        a = read(obj,"Accelerometer")
        w = read(obj,"Gyroscope")
        m = read(obj,"Magnetometer")
        t = read(obj,"Time")
        # check the label for this IMU
        congroup = open_group(obj,"Configuration")
        attr = open_attribute(congroup,"Sample Rate")
        freq = read(attr)
        period = 1/freq
        IMUarray[ifile] = IMUData(transpose(w),transpose(a),transpose(m),t,period,vec(sum(w.^2,dims=1).^0.5),vec(sum(a.^2,dims=1).^0.5))
        getdata(IMUarray[ifile],[1 size(w,2)])
        set_orientation(IMUarray[ifile],1)
    end
    close(fid)
end
maxarr = zeros(UInt64,nfiles)
for i=1:nfiles
    maxarr[i] = IMUarray[i].t[1]
end
maxind = argmax(maxarr)
m = IMUarray[maxind].t[1]
for i=1:nfiles
    ind = findfirst(x -> x >= m, IMUarray[i].t)
    IMUarray[i].t = IMUarray[i].t[ind:end]
    IMUarray[i].W = IMUarray[i].W[ind:end,:]
    IMUarray[i].A = IMUarray[i].A[ind:end,:]
    IMUarray[i].M = IMUarray[i].M[ind:end,:]
    IMUarray[i].Wm = IMUarray[i].Wm[ind:end]
    IMUarray[i].Am = IMUarray[i].Am[ind:end]
end
return IMUarray
end


function sync_apdm_bad_time(filenames,badimu,offset)
    # use this function to extract data from IMUs
    # if one IMU needs its clock set manually
    # filenames is an array of strings
    # badimu is the index in filenames of the IMU with a bad time
    # offset is how many seconds the bad one is shifted from the others
    # a positive number for offset trims that many seconds off the start of the bad IMU
    # a negative number trims the other IMUs
    # returns IMUData array in same order as filenames
    nfiles = length(filenames)
    IMUarray = Array{IMUData, 1}(undef, nfiles)
    goodimus = zeros(Int,0)
    for ifile=1:nfiles
        fid = h5open(filenames[ifile], "r")
        g = fid["Sensors"]
        for obj in g # should be only one
            a = read(obj,"Accelerometer")
            w = read(obj,"Gyroscope")
            m = read(obj,"Magnetometer")
            t = read(obj,"Time")
            # check the label for this IMU
            congroup = open_group(obj,"Configuration")
            attr = open_attribute(congroup,"Sample Rate")
            freq = read(attr)
            period = 1/freq
            IMUarray[ifile] = IMUData(transpose(w),transpose(a),transpose(m),t,period,vec(sum(w.^2,dims=1).^0.5),vec(sum(a.^2,dims=1).^0.5))
            getdata(IMUarray[ifile],[1 size(w,2)])
            set_orientation(IMUarray[ifile],1)
        end
        close(fid)
        if ifile != badimu
            append!(goodimus,ifile)
        end
    end
    maxarr = zeros(UInt64,length(goodimus))
    for i=1:length(goodimus)
        maxarr[i] = IMUarray[goodimus[i]].t[1]
    end
    maxind = argmax(maxarr)
    m = IMUarray[goodimus[maxind]].t[1] # the starting time

    if offset > 0 # shift the bad one
        ind = round(Int,offset/IMUarray[badimu].PERIOD)
        IMUarray[badimu].t = IMUarray[badimu].t[ind:end]
        IMUarray[badimu].W = IMUarray[badimu].W[ind:end,:]
        IMUarray[badimu].A = IMUarray[badimu].A[ind:end,:]
        IMUarray[badimu].M = IMUarray[badimu].M[ind:end,:]
        IMUarray[badimu].Wm = IMUarray[badimu].Wm[ind:end]
        IMUarray[badimu].Am = IMUarray[badimu].Am[ind:end]
    else # shift the others
        m += -offset*1000000
    end

    for i=1:length(goodimus)
        ind = findfirst(x -> x >= m, IMUarray[goodimus[i]].t)
        IMUarray[goodimus[i]].t = IMUarray[goodimus[i]].t[ind:end]
        IMUarray[goodimus[i]].W = IMUarray[goodimus[i]].W[ind:end,:]
        IMUarray[goodimus[i]].A = IMUarray[goodimus[i]].A[ind:end,:]
        IMUarray[goodimus[i]].M = IMUarray[goodimus[i]].M[ind:end,:]
        IMUarray[goodimus[i]].Wm = IMUarray[goodimus[i]].Wm[ind:end]
        IMUarray[goodimus[i]].Am = IMUarray[goodimus[i]].Am[ind:end]
    end
    return IMUarray
end



function detect_quiet_time(imu::IMUData)
	MAX_STATIC_RATE = 2*pi/180 # deg/sec
	MIN_STATIC_SAMPLES = 1.5/imu.PERIOD # Time converted into samples
	MAX_BIAS_SIGNAL_VAR = 2
    wm = imu.Wm

    staticCounter = 1
    staticStart = 1
    staticStartAll = zeros(0)
    staticEndAll = zeros(0)
    # maxCounter, maxStart, and maxEnd only get used if there's no static period longer than the minimum
    maxCounter = 1
    maxStart = 1
    maxEnd = 1
    for i = 1:length(wm)-1
        # look at rate of change at every point in array
        # if it's small, it's part of a potential static period
        dif = abs(wm[i+1]-wm[i])
        if (dif < MAX_STATIC_RATE)
            staticCounter+=1
        else # if it's not small, see if the previous section is long enough to be a static period
            if staticCounter > MIN_STATIC_SAMPLES
                append!( staticStartAll, staticStart )
                append!( staticEndAll,i-1)  # -1 to not include this index
            end
            if staticCounter > maxCounter # save the longest static period
                maxCounter = staticCounter
                maxStart = staticStart
                maxEnd = i
            end
            # reset for next static section
            staticCounter = 1
            staticStart = i+1
        end
    end
    # check if the final section is long enough to be a static period
    if staticCounter > MIN_STATIC_SAMPLES
        append!( staticStartAll, staticStart )
        append!( staticEndAll,length(wm)-2) # -2 to match with matlab result
    end
    if length(staticStartAll) == 0
        println("Did not find any suitable static signal; results may not be accurate")
        # if there's no suitable static signal, just take the longest
        append!( staticStartAll, maxStart )
        append!( staticEndAll, maxEnd)
    end
    staticPeriod = zeros(Int64,0)
    for i=1:length(staticStartAll)
        append!(staticPeriod,staticStartAll[i]:staticEndAll[i])
    end
    biasVar = std(wm[staticPeriod])
    biasMean = median(wm[staticPeriod])
    bestStaticInds = zeros(Int64,0)
    for i=1:length(staticPeriod)
        if abs(wm[staticPeriod[i]]-biasMean) < (biasVar*MAX_BIAS_SIGNAL_VAR)
            append!(bestStaticInds, i)
        end
    end
    staticPeriod = staticPeriod[bestStaticInds]
    return staticPeriod
end

function getdata(imu::IMUData,section,bias = nothing, optVar = nothing)
    # uses the quiet period given in bias or found automatically to subtract error from signal
    # and trims the data to the times given in section
    # bias is in seconds
    # section is in samples
    if optVar === nothing
        optVar = config()
    end
    GRAVITY = optVar.gravity
    if bias === nothing || length(bias) != 2
        staticPeriod = detect_quiet_time(imu)
    else
        staticPeriod = floor(bias[1]/imu.PERIOD):floor(bias[2]/imu.PERIOD)
    end
    if length(staticPeriod) == 0
        println("Warning: no static period found")
    else
        imu.W = imu.W .- mean(imu.W[staticPeriod,:],dims=1)
        staticAcc = imu.Am[staticPeriod]
        gravMeasure = mean(staticAcc)
        imu.A = imu.A*GRAVITY/gravMeasure
    end
    sectionSamp = section
    #sectionSamp = [convert(Int64,floor(section[1]/imu.PERIOD)), convert(Int64,floor(section[2]/imu.PERIOD))]
    if sectionSamp[1] < 1
        sectionSamp[1] = 1
    end
    if sectionSamp[2] > size(imu.W,1)
        sectionSamp[2] = size(imu.W,1)
    end
    if sectionSamp[1] == 1 # this is to make it match the matlab version
        sectionSamp[1] = 128
    end
    imu.W = imu.W[sectionSamp[1]:sectionSamp[2],:]
    imu.A = imu.A[sectionSamp[1]:sectionSamp[2],:]
    imu.M = imu.M[sectionSamp[1]:sectionSamp[2],:]
    imu.Wm = vec(sum(imu.W.^2,dims=2).^0.5)
    imu.Am = vec(sum(imu.A.^2,dims=2).^0.5)
    return nothing
end


function findwalking(leftIMU,rightIMU,waistIMU = nothing)
    # Filter to look at only periods of walking
    wMin = 300*pi/180 # magnitude of w should be above this for walking
    aMin = 20
    PERIOD = leftIMU.PERIOD
    slidingStep = convert(Int64, 1/PERIOD)
    windowSize = 512
    if waistIMU === nothing
        useWaist = false
    else
        useWaist = true
    end
    if useWaist
        N=minimum([length(leftIMU.Wm); length(rightIMU.Wm); length(waistIMU.Wm)])
    else
        N = minimum([length(leftIMU.Wm); length(rightIMU.Wm)])
    end
    periodicSection = zeros(Bool,N)
    freqs = fftfreq(windowSize, 1.0/PERIOD) |> fftshift
    x_hz = (1/PERIOD) .* (0:(windowSize/2))/windowSize

    # absolute value of all the important arrays
    leftWabs = abs.(leftIMU.W)
    rightWabs = abs.(rightIMU.W)
    if useWaist
        waistAabs = abs.(waistIMU.A)
    end
    # for each sliding step, take a small sample of waist A and left and right W
    # and determine periodicity
    for i=1:slidingStep:N-windowSize
        if i+windowSize > N
            #println(i)
        else
            if useWaist
                # determine the most sensitive axis
                if i+windowSize-1 > size(waistAabs,1)
                    println(N)
                    println(size(waistAabs,1))
                    println(i)
                end
                maxval,index = findmax([sum(waistAabs[i:i+windowSize-1,1],dims=1) sum(waistAabs[i:i+windowSize-1,2],dims=1) sum(waistAabs[i:i+windowSize-1,3],dims=1)])
                windoww = waistIMU.A[i:i+windowSize-1,index[2]]
                Fw = fft(windoww) |> fftshift
                Fwpart = abs.(Fw[convert(Int64, round(windowSize/2, digits=0)):windowSize])
                mtest,indtest = findmax(Fwpart[3:length(Fwpart)])
                maxind = indtest+2
                walkw = false
                if x_hz[maxind] > 1.2 && x_hz[maxind] <= 4
                    walkw = true
                end
            else
                walkw = true
            end 
            # left IMU
            maxval,index = findmax([sum(leftWabs[i:i+windowSize-1,1],dims=1) sum(leftWabs[i:i+windowSize-1,2],dims=1) sum(leftWabs[i:i+windowSize-1,3],dims=1)])
            windowl = leftIMU.W[i:i+windowSize-1,index[2]]
            Fl = fft(windowl) |> fftshift
            walkl = false
            m,index = findmax(abs.(Fl))
            if abs(freqs[index]) > 0.6 && abs(freqs[index]) <= 2 && m > 10*mean(abs.(Fl))
                walkl = true;
            end
            # right IMU
            maxval,index = findmax([sum(rightWabs[i:i+windowSize-1,1],dims=1) sum(rightWabs[i:i+windowSize-1,2],dims=1) sum(rightWabs[i:i+windowSize-1,3],dims=1)])
            windowr = rightIMU.W[i:i+windowSize-1,index[2]]
            Fr = fft(windowr) |> fftshift
            walkr = false
            m,index = findmax(abs.(Fr))
            if abs(freqs[index]) > 0.6 && abs(freqs[index]) <= 2 && m > 10*mean(abs.(Fr))
                walkr = true;
            end 
            # check that a and w for left and right go above the threshold   
            maxal,ind =findmax(leftIMU.Am[i:i+windowSize-1])
            maxwl,ind = findmax(leftIMU.Wm[i:i+windowSize-1])
            maxar,ind = findmax(rightIMU.Am[i:i+windowSize-1])
            maxwr,ind = findmax(rightIMU.Wm[i:i+windowSize-1])

            if walkl && walkr && walkw && maxal > aMin && maxwl > wMin && maxar > aMin && maxwr > wMin
                if i+slidingStep > N
                    periodicSection[i:N] = true
                else
                    periodicSection[i:i+windowSize] .= true
                end
            end
        end
    end
    return periodicSection
end

function mergewalking(walkSection,difSamples)
    # merges walking sections that are close together
    walkSectionNew = walkSection
    walkInds = findall(walkSection)
    difwalkInd = diff(walkInds)
    # sec = index in walkInds, difwalkInds of breaks in walking
    sec = findall(x -> x > 1, difwalkInd)
    for i = 1:length(sec)
        if difwalkInd[sec[i]] <= difSamples
            walkSectionNew[walkInds[sec[i]]:walkInds[sec[i]+1]] .= true
        end
    end
    return walkSectionNew
end

function footfall(imu::IMUData, walkSection=nothing,optVar=nothing)
    # detects footfalls based on periods where w and a are below threshold
    if optVar===nothing
        optVar = config()
    end
    walkFilter = optVar.useWalkFilter
    if !walkFilter || walkSection === nothing
        walkSection = ones(Bool,size(imu.W,1))
    end
    Am = abs.(imu.Am .- optVar.gravity)
    N = length(imu.Wm)
    tFF = optVar.tFF/imu.PERIOD
    wFF = optVar.wFF
    aFF = optVar.aFF
    stationaryPeriods = zeros(N)
    #FFstart = zeros(0)
    #FFend = zeros(0)
    if walkFilter
        # get the index of start and end of all walk sections
        walkInds = findall(walkSection)
        if isempty(walkInds)
            println("Warning: no walking section found.")
            FFstart = zeros(0)
            FFend = zeros(0)
            return FFstart,FFend,stationaryPeriods
        else
            difwalkSection = diff(walkSection)
            walkStart = findall(x -> x == 1,difwalkSection)
            walkStart = walkStart .+ 1
            walkEnd = findall(x -> x == -1,difwalkSection)
            walkEnd = walkEnd .+ 1
            if walkSection[1]
                walkStart = [1; walkStart]
            end
            if walkSection[length(walkSection)]
                append!(walkEnd,N)
            end
        end
    else # no walking filter
        walkStart = 1
        walkEnd = N
    end

    FFstart = zeros(0)
    FFend = zeros(0)
    for walki = 1:length(walkStart) # for each walking section
        #if walkStart[walki] > 2043000 && walkStart[walki] < 2483000 # use this only for s4d1
        #    wFF = optVar.wFF*2
        #    aFF = optVar.aFF*2
        #end
        #if walkStart[walki] > 3309343 && walkStart[walki] < 3479294 # use this only for s8d3
        #    wFF = optVar.wFF*2
        #    aFF = optVar.aFF*2
        #end
        N = walkEnd[walki] - walkStart[walki]
        indsW = findall(x -> x < wFF,imu.Wm[walkStart[walki]:walkEnd[walki]])
        indsA = findall(x -> x < aFF,Am[walkStart[walki]:walkEnd[walki]])
        lowMotion = intersect(indsW,indsA)
        if !isempty(lowMotion)
            # Find low motion segments separated by at least T_FF
            diflowMotion = diff(lowMotion)
            validFF = findall(x -> x > tFF, diflowMotion)
            # validFF = index numbers in diflowMotion
            lowMotionInds1  = [1; validFF.+1]
            lowMotionInds2 = [validFF; length(lowMotion)]
            startFF = lowMotion[lowMotionInds1]
            endFF = lowMotion[lowMotionInds2]
            # startFF = index numbers in W and A with low motion separated by at least tFF

            # if there's a long period of time between low motion segments,
            # increase the threshold and check again
            stepLength = (startFF[2:end] - endFF[1:end-1])*imu.PERIOD
            longs = findall(x -> x > 1.2, stepLength)  
            thisWm = imu.Wm[walkStart[walki]:walkEnd[walki]]
            thisAm = Am[walkStart[walki]:walkEnd[walki]]
            for i=1:length(longs)
                lowMotion2w = findall(x -> x < wFF*2,thisWm[round(Int,endFF[longs[i]]+tFF):round(Int,startFF[longs[i]+1]-tFF)])
                lowMotion2a = findall(x -> x < aFF*2,thisAm[round(Int,endFF[longs[i]]+tFF):round(Int,startFF[longs[i]+1]-tFF)])
                lowMotion2 = intersect(lowMotion2w,lowMotion2a)
                if length(lowMotion2) > 0 # low motion section found with higher threshold
                    diflowMotion2 = diff(lowMotion2)
                    validFF2 = findall(x -> x > tFF, diflowMotion2)
                    lowMotion2Inds1  = [1; validFF2.+1]
                    lowMotion2Inds2 = [validFF2; length(lowMotion2)]
                    # insert it into original end_FF and start_FF
                    # stick it on the end for now and sort later
                    append!(startFF,lowMotion2[lowMotion2Inds1].+ round(Int,endFF[longs[i]]+tFF-1))
                    append!(endFF,lowMotion2[lowMotion2Inds2] .+ round(Int,endFF[longs[i]]+tFF-1))
                end
            end
            startFF = sort(startFF)
            endFF = sort(endFF)

            # Find best FF point
            FFsize = length(startFF)
            #FFindex = zeros(0)
            for ffi = 1:FFsize # for each low motion period
                if walki == 1 && ffi == 1 # first step
                    FFcut = endFF[ffi]
                    # or take half the length of a normal footfall
                    if FFsize > 2
                        FFcut = endFF[ffi] - floor(mean(endFF[2:end-1]-startFF[2:end-1])/2)
                        # but don't let it come before the start of the footfall section
                        if FFcut < startFF[ffi]
                            FFcut = startFF[ffi]   
                        end
                    end
    
                elseif ffi == FFsize && walki == length(walkStart) # last step
                    FFcut = startFF[ffi]
                    if FFsize > 2
                        FFcut = startFF[ffi] + floor(mean(endFF[2:end-1]-startFF[2:end-1])/2)
                        if FFcut > endFF[ffi]
                            FFcut = endFF[ffi]   
                        end
                    end

                elseif (endFF[ffi] - startFF[ffi]) > optVar.maxTFF/imu.PERIOD # split into 2 FFs
                    if (ffi == 1)
                        FFcut = endFF[ffi]-floor(optVar.maxTFF/imu.PERIOD/2)
                    elseif (ffi == FFsize)
                        FFcut = startFF[ffi]+floor(optVar.maxTFF/imu.PERIOD/2)
                    else
                        FFcut = [startFF[ffi]+floor(optVar.maxTFF/imu.PERIOD/2); endFF[ffi]-floor(optVar.maxTFF/imu.PERIOD/2)]
                    end
                else # for most steps, use middle of low motion segment
                    FFcut = startFF[ffi]+floor((endFF[ffi]-startFF[ffi])/2)
                end
                if length(FFcut) == 1
                    append!(FFstart,FFcut+walkStart[walki]-1)
                    if ffi > 1 # first FF is start but not end
                        append!(FFend,FFcut+walkStart[walki]-1)
                    end
                else # multiple FFs
                    append!(FFstart,FFcut[2]+walkStart[walki]-1)
                    append!(FFend,FFcut[1]+walkStart[walki]-1)
                end
            end
            if length(FFstart) > length(FFend)
                pop!(FFstart) # last FF cannot be start of step
            end
        end
    end
    # Now find low dynamic conditions over the whole time period to set stationaryPeriods
    indsW = findall(x -> x < optVar.wFF,imu.Wm[1:length(imu.Wm)])
    indsA = findall(x -> x < optVar.aFF,Am[1:length(Am)])
    lowMotion = intersect(indsW,indsA)
    stationaryPeriods[lowMotion] .= 1
    FFstart = convert(Array{Int64,1},FFstart)
    FFend = convert(Array{Int64,1},FFend)
    return FFstart,FFend,stationaryPeriods
end


function qua_est(W1,quaternionPrev)
    # applies a rotation to a quaternion
    mag=(sum(W1.^2)).^.5
    if mag != 0
        sin_mag = sin(mag/2.0)/mag
    else
        sin_mag = 0.5
    end
    rotation = [cos(mag/2.0); sin_mag*W1]
    a = quaternionPrev[1]
    b = quaternionPrev[2] 
    c = quaternionPrev[3] 
    d = quaternionPrev[4]
    quaternion_sqw = [a -b -c -d;  b a -d c;  c d a -b;  d -c b a] 
    quaternion = quaternion_sqw*rotation
    return quaternion
end

function qua2rot(quaternion)
    # converts quaternion to rotation matrix
    a = quaternion[1]
    b = quaternion[2]
    c = quaternion[3]
    d = quaternion[4]
    rotMat = zeros(3,3)
    rotMat[1,1] = a^2 + b^2 - c^2 - d^2
    rotMat[1,2] = 2*(b*c - a*d)
    rotMat[1,3] = 2*(b*d + a*c)

    rotMat[2,1] = 2*(b*c + a*d)
    rotMat[2,2] = a^2 - b^2 + c^2 - d^2
    rotMat[2,3] = 2*(c*d - a*b)

    rotMat[3,1] = 2*(b*d - a*c)
    rotMat[3,2] = 2*(c*d + a*b)
    rotMat[3,3] = a^2 - b^2 - c^2 + d^2
    return rotMat
end

function eul2qua(Att)
    phi=Att[1]
    the=Att[2]
    psi=Att[3]
    cos_phi=cos(phi/2.0)
    sin_phi=sin(phi/2.0)
    cos_the=cos(the/2.0)
    sin_the=sin(the/2.0)
    cos_psi=cos(psi/2.0)    
    sin_psi=sin(psi/2.0)
    a=cos_phi.*cos_the.*cos_psi+sin_phi.*sin_the.*sin_psi
    b=sin_phi.*cos_the.*cos_psi-cos_phi.*sin_the.*sin_psi
    c=cos_phi.*sin_the.*cos_psi+sin_phi.*cos_the.*sin_psi
    d=cos_phi.*cos_the.*sin_psi-sin_phi.*sin_the.*cos_psi
    Q=[a,b,c,d]
    return Q
end

function qua2eul(quaternion::Array{Float64,2})
    a = quaternion[:,1]
    b = quaternion[:,2]
    c = quaternion[:,3]
    d = quaternion[:,4]
    phi = atan.((2*(a.*b + c.*d)),(a.^2 - b.^2 - c.^2 + d.^2))
    the = asin.(2*(a.*c - d.*b))
    psi = atan.((2*(a.*d + b.*c)),(a.^2 + b.^2 - c.^2 - d.^2))
    euler = [phi the psi]
    return euler
end

function qua2eul(quaternion::Array{Float64,1})
    a = quaternion[1]
    b = quaternion[2]
    c = quaternion[3]
    d = quaternion[4]
    phi = atan((2*(a*b + c*d)),(a^2 - b^2 - c^2 + d^2))
    the = asin(2*(a*c - d*b))
    psi = atan((2*(a*d + b*c)),(a^2 + b^2 - c^2 - d^2))
    euler = [phi the psi]
    return euler
end

function acctilt(A,grav)
    # Compute tilt from acceleration
    AX = A[:,1]
    AY = A[:,2]
    # Compute pitch 
    accel_theta = (AX)/grav
    valid_angles = findall(x -> x <= 1, abs.(accel_theta))
    accel_theta[valid_angles] = asin.(accel_theta[valid_angles])
    # Compute roll angle
    accel_phi = (AY)./cos.(accel_theta)/grav
    valid_angles = findall(x -> x <= 1, abs.(accel_phi))
    accel_phi[valid_angles] = -asin.(accel_phi[valid_angles])
    return accel_phi,accel_theta
end

function computepos(imu::IMUData, walkSection=nothing,optVar=nothing)
    if optVar===nothing
        optVar = config()
    end
    walkFilter = optVar.useWalkFilter
    if !walkFilter || walkSection === nothing
        walkSection = ones(Bool,length(imu.W))
    end

    returnVal = footfall(imu, walkSection, optVar)
    FFstart = returnVal[1]
    FFend = returnVal[2]
    stationaryPeriods = returnVal[3]
    N = length(imu.Wm)
    returnVal = acctilt(imu.A,optVar.gravity)
    accelPhi = returnVal[1]
    accelTheta = returnVal[2]
    # Initialize variables
	quaternion = zeros(N,4)
	An = zeros(N,3)
	Anz = zeros(N,3)
	# Initialize quaternions and KF
    P = 0 # Initial covariance error
    Q = 1.5e-5*imu.PERIOD # Process (gyros) covariance error
    R = 1.5e-1*imu.PERIOD # Measurement (accelerometer) covariance error
    quaternion[1,1] = 1 # quaternion starts at 1,0,0,0
    lastFF = 0
    for iframe = 2:N 
        # Compute attitude using quaternion representation
        quaternion[iframe,:] = qua_est(imu.W[iframe,:],quaternion[iframe-1,:]) # qua_est estimates the next step based on previous quaterion and angular velocity
        rotMat = qua2rot(quaternion[iframe,:]) # make rotation matrix based on current orientation
        An[iframe,:] = rotMat*imu.A[iframe,:] # apply rotation to accelerations
        if optVar.useKF # apply kalman filter
            # Propagate covariance error
            P = P + Q
            if stationaryPeriods[iframe]==1
                # Compute Kalman gain
                K = P/(P + R)
                euler = qua2eul(quaternion[iframe,:])
                # Apply corrections to the state
                quaternion[iframe,:] = eul2qua([euler[1] - (euler[1] - accelPhi[iframe])*K,euler[2] - (euler[2] - accelTheta[iframe])*K,euler[3]])
                # Update covariance error
                P = (1 - K)*P*(1 - K) + K*R*K
            end
        end
        # Apply zero velocity update
        if iframe in FFstart || iframe in FFend         # on foot falls only
            stepRange = lastFF+1:iframe
            stepSamples = length(stepRange)
            # skip short steps
            if stepSamples > 2
                vError = sum(An[stepRange,:],dims=1)
                # Compute the accelerometer error assuming it is linear
                aError = vError/stepSamples
                Anz[stepRange,:] = An[stepRange,:] .- aError
                lastFF = iframe
            end
        end
    end
    V = cumsum(Anz,dims=1)*imu.PERIOD
    Vm = (sum(V[:,1:2].^2,dims=2)).^.5
    Vm =  Vm[:,1] 
    P1 = cumsum(V,dims=1)*imu.PERIOD
    result = WalkData(FFstart,FFend,walkSection,Anz,An,imu.A,imu.W,V,Vm,P1,quaternion,imu.PERIOD)
    return result
end


function finddist(P1::Array{Float64,2},P2::Array{Float64,2},rotation)
    rot = [cos(rotation) -sin(rotation) 0; sin(rotation) cos(rotation) 0; 0 0 1]
    P2 = P2*rot    
    dist = 0;
    for i=1:size(P1)[1]
        dist = dist+(P1[i,1] - P2[i,1])^2 + (P1[i,2] - P2[i,2])^2 + (P1[i,3] - P2[i,3])^2
    end
    return dist
end

function getsteps(walkinfo::WalkData,optVar = nothing)
    if optVar===nothing
        optVar = config()
    end
    DIRECTION_STEPS = optVar.dirSteps
    FFstart = walkinfo.FFstart
    FFend = walkinfo.FFend
    nsteps = length(FFstart)
    maxlength,ind = findmax(FFend - FFstart)
    maxlength = maxlength+1
    P = walkinfo.P
    euler = qua2eul(walkinfo.quaternion)

    # Create matrices to store results
	ltrlSwing = zeros(nsteps,maxlength)
	frwdSwing = zeros(nsteps,maxlength)
	ltrl = zeros(nsteps,maxlength)
	frwd = zeros(nsteps,maxlength)
	absLtrl = zeros(nsteps,maxlength)
	absFrwd = zeros(nsteps,maxlength)
	elev = zeros(nsteps,maxlength)
	theta = zeros(nsteps,maxlength)
	footHeading = zeros(nsteps)
	diffFootHeading = zeros(nsteps)
	startEnd = zeros(nsteps,2)
    stepSamples = zeros(nsteps)

    # Compute individual step direction
	direction = atan.(P[FFend,2] - P[FFstart,2],P[FFend,1] - P[FFstart,1])
    # get the index of start and end of all walk sections
    walkSection = walkinfo.walkSection
    walkInds = findall(walkSection)
    if isempty(walkInds)
        println("Warning: no walking section found.")
        result = StrideData(zeros(0,0),zeros(0,0),zeros(0,0),zeros(0,0),zeros(0,0),zeros(0,0),zeros(0,0),zeros(0,0),zeros(0,0),zeros(0),zeros(0),zeros(0),zeros(0),zeros(0),zeros(0)) 
        return result
    end
    difwalkSection = diff(walkSection)
    walkStart = findall(x -> x == 1,difwalkSection)
    walkStart = walkStart .+ 1
    walkEnd = findall(x -> x == -1,difwalkSection)
    walkEnd = walkEnd .+ 1
    if walkSection[1]
        walkStart = [1; walkStart]
    end
    if walkSection[length(walkSection)]
        append!(walkEnd,length(walkSection))
    end
    ws = walkinfo.quaternion[FFstart,1]
    xs = walkinfo.quaternion[FFstart,2]
    ys = walkinfo.quaternion[FFstart,3]
    zs = walkinfo.quaternion[FFstart,4]
    fhs = atan.((2 .* ws .* zs) + (2 .* xs .* ys),1 .- (2 .* (( ys .* ys)+(zs .* zs))))
    Pxs = P[FFend,1]-P[FFstart,1]
    Pys = P[FFend,2]-P[FFstart,2]
    stepHeading = atan.(Pys,Pxs)
    dist = sqrt.((Pxs.*Pxs)+(Pys.*Pys))
    angsave = zeros(nsteps) # added for testing
    for i = 1:nsteps
        frwdSwing[i,1:FFend[i] - FFstart[i] + 1] .= P[FFstart[i]:FFend[i],1]*cos(-direction[i]) .- P[FFstart[i]:FFend[i],2].*sin(-direction[i])
		ltrlSwing[i,1:FFend[i] - FFstart[i] + 1] .= P[FFstart[i]:FFend[i],1]*sin(-direction[i]) .+ P[FFstart[i]:FFend[i],2].*cos(-direction[i])
        if FFend[i] - FFstart[i] + 2 < maxlength
            frwdSwing[i,FFend[i] - FFstart[i] + 2:maxlength] .= frwdSwing[i,FFend[i] - FFstart[i] + 1]
		    ltrlSwing[i,FFend[i] - FFstart[i] + 2:maxlength] .= ltrlSwing[i,FFend[i] - FFstart[i] + 1]
        end
        # find steps in this walking section
        ps_start_smaller = findall(x -> x <= FFstart[i], walkStart)
        if length(ps_start_smaller) == 0
            sectionStart = walkStart[1]
        else
            sectionStart = walkStart[ps_start_smaller[length(ps_start_smaller)]] # where this walking section starts
        end
        ps_end_larger = findall(x -> x >= FFend[i], walkEnd)
        if length(ps_end_larger) == 0
            sectionEnd = walkEnd[1]
        else
            sectionEnd = walkEnd[ps_end_larger[1]] # where this walking section ends
        end
        step_inds = findall(x -> x >= sectionStart, FFstart)
        firstStep = step_inds[1] # first step number in section
        step_inds = findall(x -> x <= sectionEnd, FFend)
        lastStep = step_inds[end] # last step number in section
        # Select the steps +/- DIRECTION_STEPS
        if (i-DIRECTION_STEPS >= firstStep && i+DIRECTION_STEPS <= lastStep)
            nearbyStepsInd = (i - DIRECTION_STEPS):(i + DIRECTION_STEPS)
        elseif (i-DIRECTION_STEPS < firstStep && i+DIRECTION_STEPS <= lastStep)
            nearbyStepsInd = (firstStep):(i+DIRECTION_STEPS)
        elseif (i-DIRECTION_STEPS >= firstStep && i+DIRECTION_STEPS > lastStep)
            nearbyStepsInd = (i - DIRECTION_STEPS):(lastStep)
        else
            nearbyStepsInd = firstStep:lastStep
        end
        if length(nearbyStepsInd) > length(FFstart)
            nearbyStepsInd = nearbyStepsInd[1:length(FFstart)] 
        end
        # Find local direction of travel
        numNearbySteps = length(nearbyStepsInd)
        Pvx = zeros(numNearbySteps+1)
        Pvy = zeros(numNearbySteps+1)
        Pvx[1:numNearbySteps] .= P[FFstart[nearbyStepsInd],1] .- P[FFstart[nearbyStepsInd[1]],1]
        Pvy[1:numNearbySteps] .= P[FFstart[nearbyStepsInd],2] .- P[FFstart[nearbyStepsInd[1]],2]
        Pvx[numNearbySteps+1] = P[FFend[nearbyStepsInd[numNearbySteps]],1]-P[FFstart[nearbyStepsInd[1]],1]
        Pvy[numNearbySteps+1] = P[FFend[nearbyStepsInd[numNearbySteps]],2]-P[FFstart[nearbyStepsInd[1]],2]
        Pqx = cumsum([0; dist[nearbyStepsInd].*cos.(fhs[nearbyStepsInd])])
        Pqy = cumsum([0; dist[nearbyStepsInd].*sin.(fhs[nearbyStepsInd])])
        # Pv = foot position from walkinfo.P
        # Pq = P if the step direction for every step is the foot heading in walkinfo.quaternion
        Pq = [Pqx Pqy zeros(size(Pqx))]
        Pv = [Pvx Pvy zeros(size(Pvx))]
        footHeading[i] = fhs[i]
        # rotate the foot heading for this step so that on average,
        # foot headings for all nearby steps align with step direction

        # set up optimization
        #opt = Model(optimizer_with_attributes(Ipopt.Optimizer, "print_level"=>0))
        #@variable(opt, x)
        rotfun(x) = finddist(Pv,Pq,x)
        #register(opt, :rotfun, 1, rotfun, autodiff=true)
        #@NLobjective(opt, Min, rotfun(x))

        result = optimize(rotfun, 0., 6.)

        #Pv1 = Pv
        #Pq1 = Pq
        #@NLparameter(opt,Pv1 == Pv)
        #@NLparameter(opt,Pq1 == Pq)
        #set_value(Pv1,Pv)
        #set_value(Pq1,Pq)
        #optimize!(opt)
        if Optim.converged(result)
            ang = Optim.minimizer(result)
            while ang > pi
                ang = ang-(2*pi)
            end
            while ang < -pi
                ang = ang+(2*pi);
            end
        else
            ang = 0
            println("Step direction unknown")
        end
        footHeading[i] = footHeading[i]-ang
        # make sure foot heading is between pi and -pi
        while footHeading[i] > pi
            footHeading[i] = footHeading[i]-(2*pi)
        end
        while footHeading[i] < -pi
            footHeading[i] = footHeading[i]+(2*pi);
        end     
        # rotate this section of Px and Py to go into frwd and ltrl
        angsave[i] = ang # added for testing
        rotMat = [cos(-footHeading[i]) sin(-footHeading[i]); -sin(-footHeading[i]) cos(-footHeading[i])]
        rotP = [P[FFstart[i]:FFend[i],1] P[FFstart[i]:FFend[i],2]] * rotMat
        frwd[i, 1:FFend[i] - FFstart[i] + 1] = rotP[:,1]
        ltrl[i, 1:FFend[i] - FFstart[i] + 1] = rotP[:,2]
        # store elevation information
        elev[i,1:FFend[i] - FFstart[i] + 1] = P[FFstart[i]:FFend[i],3] 
        # pitch angle
        theta[i,1:FFend[i] - FFstart[i] + 1] = euler[FFstart[i]:FFend[i],2]
        # fill in the end with whatever the last point in this step is 
        if FFend[i] - FFstart[i] + 2 < maxlength
            frwd[i,FFend[i] - FFstart[i] + 2:maxlength] .= frwd[i,FFend[i] - FFstart[i] + 1]
            ltrl[i,FFend[i] - FFstart[i] + 2:maxlength] .= ltrl[i,FFend[i] - FFstart[i] + 1]
            elev[i,FFend[i] - FFstart[i] + 2:maxlength] .= elev[i,FFend[i] - FFstart[i] + 1]
            theta[i,FFend[i] - FFstart[i] + 2:maxlength] .= theta[i,FFend[i] - FFstart[i] + 1]
        end
        # heading angle
        diffFootHeading[i] =  euler[FFend[i],3]-euler[FFstart[i],3]
        startEnd[i,:] = [FFstart[i] FFend[i]]
        stepSamples[i] = FFend[i] - FFstart[i]
    end
    # Translate foot fall location to the origin
    frwdSwing = frwdSwing - frwdSwing[:,1] * ones(1,maxlength)
    absFrwd = frwd
    frwd = frwd - frwd[:,1] * ones(1,maxlength)
    ltrlSwing = ltrlSwing - ltrlSwing[:,1] * ones(1,maxlength)
    absLtrl = ltrl
    ltrl = ltrl - ltrl[:,1] * ones(1,maxlength)
    elev = elev - elev[:,1] * ones(1,maxlength)
    # Compute step speed
    stepLength = frwd[:,maxlength]
    time = stepSamples .* walkinfo.PERIOD
    stepSpeed = stepLength./time
    result = StrideData(frwdSwing,ltrlSwing,frwd,ltrl,absLtrl,absFrwd,elev,theta,startEnd,footHeading-stepHeading,diffFootHeading,stepSamples,time,stepSpeed,stepLength)
    return result
end

function findmissedsteps(imu::IMUData, strides::StrideData, walkinfo::WalkData, optVar = nothing)
    if optVar===nothing
        optVar = config()
    end
    # check if any steps are very long
    longinds = findall(x -> abs(x) > 2, strides.length)
    if !isempty(longinds)
        Anz = walkinfo.Anz
        FFstart = walkinfo.FFstart
        FFend = walkinfo.FFend
        wFF = optVar.wFF * 3
        aFF = optVar.aFF * 3
        tFF = round(Int,0.5 * optVar.tFF/imu.PERIOD) 
        Am = abs.(imu.Am .- optVar.gravity)
        count = 0 # how many new FFs get added
        for li=1:length(longinds) # for each long step
            # check for extra footfalls within this step
            thisstep1 = Int(strides.startEnd[longinds[li],1]) # where this step starts
            thisstep2 = Int(strides.startEnd[longinds[li],2]) # where this step ends
            if thisstep2-thisstep1 > 2*tFF
                indsW = findall(x -> x < wFF,imu.Wm[thisstep1+tFF:thisstep2-tFF])
                indsA = findall(x -> x < aFF,Am[thisstep1+tFF:thisstep2-tFF])
                lowMotion = intersect(indsW,indsA)
                if !isempty(lowMotion) # there's at least one new ff to add 
                    # Find low motion segments separated by at least T_FF
                    newFF = zeros(Int64,0)
                    diflowMotion = diff(lowMotion)
                    validFF = findall(x -> x > tFF, diflowMotion)
                    # validFF = index numbers in diflowMotion
                    lowMotionInds1  = [1; validFF.+1]
                    lowMotionInds2 = [validFF; length(lowMotion)]
                    startFF = lowMotion[lowMotionInds1]
                    endFF = lowMotion[lowMotionInds2]
                    # startFF = index numbers in W and A with low motion separated by at least tFF

                    # Find best FF point and add it to FFstart and FFend
                    FFsize = length(startFF)
                    for ffi = 1:FFsize # for each low motion period
                        FFcut = startFF[ffi]+floor((endFF[ffi]-startFF[ffi])/2) + thisstep1+tFF-1
                        append!(newFF,FFcut)
                    end
                    # Make new Anz with zero velocity update
                    lastFF = thisstep1
                    for i=1:length(newFF)
                        stepRange = lastFF+1:newFF[i]
                        stepSamples = length(stepRange)
                        # skip short steps
                        if stepSamples > 2
                            vError = sum(walkinfo.An[stepRange,:],dims=1)
                            # Compute the accelerometer error assuming it is linear
                            aError = vError/stepSamples
                            Anz[stepRange,:] = walkinfo.An[stepRange,:] .- aError
                            lastFF = newFF[i]
                        end
                    end
                    for i=1:length(newFF)
                        insert!(FFstart,longinds[li]+count+1,newFF[i])
                        insert!(FFend,longinds[li]+count,newFF[i])
                        count = count+1
                    end                    

                end
            end
        end
        # make new V and P
        if count > 0
            V = cumsum(Anz,dims=1)*imu.PERIOD
            Vm = (sum(V[:,1:2].^2,dims=2)).^.5
            Vm =  Vm[:,1] 
            P1 = cumsum(V,dims=1)*imu.PERIOD
            walkinfo.FFstart = FFstart
            walkinfo.FFend = FFend
            walkinfo.Anz = Anz
            walkinfo.V = V
            walkinfo.Vm = Vm
            walkinfo.P = P1
            # find new strides
            strides = getsteps(walkinfo, optVar)
        end
    end
    return strides
end



function DTWonefoot(walkinfo::WalkData, strides::StrideData, threshfile = "",optVar = nothing)
    # runs the DTW algorithm on W,A of one foot to find the start and end point of each potential stride
    if optVar===nothing
        optVar = config()
    end
    # first find the longest walking bout
    maxRest = 4/walkinfo.PERIOD
    maxstart = 1
    indstart = 1
    maxstrides = 1
    stepCount = 1
    for i=2:size(strides.startEnd,1)
        if strides.startEnd[i,1] - strides.startEnd[i-1,2] <= maxRest
            stepCount = stepCount+1
        else
            if stepCount > maxstrides
                maxstrides = stepCount
                maxstart = indstart
            end
            stepCount = 1
            indstart = i
        end
    end
    # resample and average together strides to make a template
    stridesw = zeros(200,maxstrides,3)
    stridesa = zeros(200,maxstrides,3)


    for i=1:maxstrides
        for j=1:3
            lw1 = walkinfo.W[Int(strides.startEnd[maxstart+i-1,1]):Int(strides.startEnd[maxstart+i-1,2]),j]
            lw1resamp = imresize(lw1, 200)
            stridesw[:,i,j] = lw1resamp/walkinfo.PERIOD*180/pi*1/2000
            la1 = walkinfo.A[Int(strides.startEnd[maxstart+i-1,1]):Int(strides.startEnd[maxstart+i-1,2]),j]
            la1resamp = imresize(la1, 200)
            stridesa[:,i,j] = la1resamp/(16*9.8)
        end
    end

    templatew = mean(stridesw,dims=2)
    templatea = mean(stridesa,dims=2)
    tw1 = vec(templatew[:,:,1])
    tw2 = vec(templatew[:,:,2])
    tw3 = vec(templatew[:,:,3])
    ta1 = vec(templatea[:,:,1])
    ta2 = vec(templatea[:,:,2])
    ta3 = vec(templatea[:,:,3])

    W = walkinfo.W/walkinfo.PERIOD*180/pi*1/2000   #.*(180/(2000*walkinfo.PERIOD))    
    A = walkinfo.A./(16*9.8)
    d = zeros(200,size(W,1))
    println("calculating distance matrix")
    for i=1:200
        for j=1:size(W,1)
            d[i,j] = sqrt((tw1[i]-W[j,1])^2) + sqrt((tw2[i]-W[j,2])^2) + sqrt((tw3[i]-W[j,3])^2) + sqrt((ta1[i]-A[j,1])^2) + sqrt((ta2[i]-A[j,2])^2) + sqrt((ta3[i]-A[j,3])^2)
        end
    end

    #c = zeros(size(d))
    # reset d to be used as c matrix instead of allocating a new array
    #d[1,:] = d[1,:]
    d[:,1] = cumsum(d[:,1])
    prevstart = 1:size(d,2)
    thisstart = zeros(size(d,2),1)
    println("calculating cost matrix")
    for i=2:size(d,1)
        thisstart = zeros(size(d,2),1)
        #fill!(thisstart,0)
        thisstart[1] = 1
        for j=2:size(d,2)
            m = minimum([d[i-1,j] d[i-1,j-1] d[i,j-1]])
            ind = argmin([d[i-1,j] d[i-1,j-1] d[i,j-1]])
            start1 = [prevstart[j] prevstart[j-1] thisstart[j-1]]
            d[i,j] = d[i,j] + m
            thisstart[j] = start1[ind]
        end
        prevstart = thisstart
    end
    #return c[end,:]
    # peaks in -c[end,:] tell you endpoint of potential strides
    # find average height of peaks in known walking section
    # and use that to set the threshold
    # ind1 = Int(strides.startEnd[maxstart,1])
    # ind2 = Int(strides.startEnd[maxstart+maxstrides-1,2])
    # pkindices, properties = findpeaks1d(d[end,ind1:ind2],width=tFF/walkinfo.PERIOD)
    # highmean = mean(d[end,pkindices.+(ind1-1)])
    # negpkindices, properties = findpeaks1d(-d[end,ind1:ind2],width=tFF/walkinfo.PERIOD)
    # lowmean = mean(d[end,negpkindices.+(ind1-1)])
    # #thresh = 0.8*(highmean-lowmean) # lower thresh = more potential strides
    # thresh1 = -(0.5*(highmean-lowmean) +lowmean)
    # println(thresh1)
    endpoint = zeros(UInt64,0)
    startpoint = zeros(UInt64,0)
    threshs = zeros(0)
    if threshfile == "" # enter one or multiple thresholds
        plot(imresize(d[end,:],2000),xticks = 0:100:2000,legend=false,show=true)
        plot!(size=(1600,400))
        png("distFunction.png")
        nthreshs = 1
        println("Enter indices where new threshold should start. Or, enter 0 if only one threshold is needed.")
        tstring = readline()
        tinds = [parse(Int, ss) for ss in split(tstring)]
        if length(tinds) == 1 && tinds[1] == 0
            println("Enter a threshold")
            tstring = readline()
            thresh = (parse(Float64,tstring))
            println(thresh)
            minx, properties = findpeaks1d(-d[end,:],width=optVar.tFF/walkinfo.PERIOD,height=-thresh)  
            endpoint = minx
            startpoint = thisstart[minx]
            append!(threshs,thresh)
        else
            nthreshs = length(tinds)+1
            println("Enter ", string(nthreshs)," thresholds")
            tstring = readline()
            threshs = [parse(Float64, ss) for ss in split(tstring)]
            while length(threshs) < nthreshs
                tstring = readline()
                threshs1 = [parse(Float64, ss) for ss in split(tstring)]
                threshs = [threshs; threshs1]
            end
            append!(tinds,2000)
            println(tinds)
            println(threshs)
            ind0 = 1
            for i=1:nthreshs
                ind1 = round(Int,tinds[i]*size(d,2)/2000)
                minx, properties = findpeaks1d(-d[end,ind0:ind1],width=optVar.tFF/walkinfo.PERIOD,height=-threshs[i])  
                minx = minx .+ (ind0-1)
                endpoint = [endpoint; minx]
                startpoint = [startpoint; thisstart[minx]]
                ind0 = ind1+1
            end  
    
        end
    else # read thresholds from file
        rt = readdlm(threshfile, ',', Float64)
        tinds = Int.(rt[:,1])
        threshs = rt[:,2]
        if length(tinds) == 1 && tinds[1] == 0
            minx, properties = findpeaks1d(-d[end,:],width=optVar.tFF/walkinfo.PERIOD,height=-threshs[1])  
            endpoint = minx
            startpoint = thisstart[minx]
        else
            nthreshs = length(tinds)
            ind0 = 1
            for i=1:nthreshs
                ind1 = round(Int,tinds[i]*size(d,2)/2000)
                minx, properties = findpeaks1d(-d[end,ind0:ind1],width=optVar.tFF/walkinfo.PERIOD,height=-threshs[i])  
                minx = minx.+(ind0-1)
                endpoint = [endpoint; minx]
                startpoint = [startpoint; thisstart[minx]]
                ind0 = ind1+1
            end 
        end
    end





    # delete overlapping steps
    maxoverlap = 12
    if length(startpoint) == 0
        println("no steps found")
        # result = DTWvar
    else
        #prevstep = Int(startpoint[1]):Int(endpoint[1])
        prevend = Int(endpoint[1])
        prevind = 1

        stepind = zeros(Int,0)
        newstartpoint = zeros(0)
        newendpoint = zeros(0)
        println("checking for overlapping steps")
        for i=2:length(endpoint)
            #thisstep = startpoint[i]:endpoint[i]
            #overlap = length(findall(in(prevstep),thisstep))
            if (prevend - startpoint[i]) > maxoverlap # if steps overlap, only keep one
                if d[end,endpoint[i]] < d[end,prevind] # replace prev step with this one
                    #prevstep = thisstep
                    prevend = endpoint[i]
                    prevind = i
                end
            else # steps don't overlap so keep both
                append!(stepind,prevind)
                append!(newstartpoint,startpoint[prevind])
                append!(newendpoint,endpoint[prevind])
                #prevstep = thisstep
                prevend = endpoint[i]
                prevind = i
            end
        end
        append!(stepind,prevind)
        append!(newstartpoint,startpoint[prevind])
        append!(newendpoint,endpoint[prevind])
        result = DTWvar(tw1,tw2,tw3,ta1,ta2,ta3,newstartpoint,newendpoint,d[end,:],zeros(Bool,size(walkinfo.walkSection)),tinds,threshs)
        return result
    end

end

function DTWfindwalking(leftwalkinfo::WalkData, leftstrides::StrideData, rightwalkinfo::WalkData, rightstrides::StrideData; threshfilel = "", threshfiler = "",optVar = nothing)
    if optVar === nothing
        optVar = config()
    end
    println("Starting DTW process for left foot")
    DTWleft = DTWonefoot(leftwalkinfo,leftstrides,threshfilel,optVar)
    println("Starting DTW process for right foot")
    DTWright = DTWonefoot(rightwalkinfo,rightstrides,threshfiler,optVar)
    leftSection = zeros(Bool,size(leftwalkinfo.walkSection))
    rightSection = zeros(Bool,size(rightwalkinfo.walkSection))
    if length(DTWleft.startpoint) == 0 || length(DTWright.startpoint) == 0
        println("no steps found")
        return [DTWleft DTWright]
    end
    # each step has to be within 1 second of a step on the other foot

    inds = zeros(Int,0) # indices of left foot steps to keep
    indsr = zeros(Int,0) # indices of right foot steps to keep
    for i=1:length(DTWleft.startpoint)
        first = findfirst(DTWright.startpoint .> DTWleft.startpoint[i] - 1/leftwalkinfo.PERIOD)
        last = findlast(DTWright.startpoint .< DTWleft.startpoint[i] + 1/leftwalkinfo.PERIOD)
        if first !== nothing && last !== nothing
            if first <= last # there is at least one step within 1 second of this one
                append!(inds,i)
                append!(indsr,first:last)
            end
        end
    end
    stepstartr = DTWright.startpoint[unique(indsr)]
    stependr = DTWright.endpoint[unique(indsr)]
    stepstartl = DTWleft.startpoint[inds]
    stependl = DTWleft.endpoint[inds]

    # walking sections are where steps are less than 4 seconds apart (or optVar.maxWalkDif seconds)
    stepdif = stepstartl[2:end] - stependl[1:end-1]
    walkind = findall(x -> x > optVar.maxWalkDif/leftwalkinfo.PERIOD, stepdif)
    psstart = [stepstartl[1]; stepstartl[walkind.+1]]
    psend = [stependl[walkind]; stependl[end]]
    for i=1:length(psstart)
        leftSection[psstart[i]:psend[i]] .= 1
    end
    # same thing on right foot
    stepdif = stepstartr[2:end] - stependr[1:end-1]
    walkind = findall(x -> x > optVar.maxWalkDif/rightwalkinfo.PERIOD, stepdif)
    psstart = [stepstartr[1]; stepstartr[walkind.+1]]
    psend = [stependr[walkind]; stependr[end]]
    for i=1:length(psstart)
        rightSection[psstart[i]:psend[i]] .= 1
    end   
    
    DTWleft.walkSection = leftSection
    DTWright.walkSection = rightSection
    return [DTWleft DTWright]

end

function DTWpath(walkinfo::WalkData, strides::StrideData,teststart,testend,stridenum)
    # returns the warping path used for one stride found with DTW
    # used for debugging
    # teststart = start index of data to use (default 1)
    # testend = endindex of data to use (default length of w or a)
    # stridenum = which stride to get the warping path for (default 1)
    tFF = 0.4 # time between footsteps
    if teststart===nothing
        teststart = 1
    end
    if testend === nothing
        testend = size(walkinfo.W,1)
    end
    if stridenum === nothing
        stridenum = 1
    end
    # use config struct?
    # first find the longest walking bout
    maxRest = 4/walkinfo.PERIOD
    maxstart = 1
    indstart = 1
    maxstrides = 1
    stepCount = 1
    for i=2:size(strides.startEnd,1)
        if strides.startEnd[i,1] - strides.startEnd[i-1,2] <= maxRest
            stepCount = stepCount+1
        else
            if stepCount > maxstrides
                maxstrides = stepCount
                maxstart = indstart
            end
            stepCount = 1
            indstart = i
        end
    end
    # resample and average together strides to make a template
    stridesw = zeros(200,maxstrides,3)
    stridesa = zeros(200,maxstrides,3)


    for i=1:maxstrides
        for j=1:3
            lw1 = walkinfo.W[Int(strides.startEnd[maxstart+i-1,1]):Int(strides.startEnd[maxstart+i-1,2]),j]
            lw1resamp = imresize(lw1, 200)
            stridesw[:,i,j] = lw1resamp/walkinfo.PERIOD*180/pi*1/2000
            la1 = walkinfo.A[Int(strides.startEnd[maxstart+i-1,1]):Int(strides.startEnd[maxstart+i-1,2]),j]
            la1resamp = imresize(la1, 200)
            stridesa[:,i,j] = la1resamp/(16*9.8)
        end
    end

    templatew = mean(stridesw,dims=2)
    templatea = mean(stridesa,dims=2)
    tw1 = vec(templatew[:,:,1])
    tw2 = vec(templatew[:,:,2])
    tw3 = vec(templatew[:,:,3])
    ta1 = vec(templatea[:,:,1])
    ta2 = vec(templatea[:,:,2])
    ta3 = vec(templatea[:,:,3])

    W = walkinfo.W[teststart:testend,:]/walkinfo.PERIOD*180/pi*1/2000   #.*(180/(2000*walkinfo.PERIOD))    
    A = walkinfo.A[teststart:testend,:]./(16*9.8)
    d = zeros(200,size(W,1))
    println("calculating distance matrix")
    for i=1:200
        for j=1:size(W,1)
            d[i,j] = sqrt((tw1[i]-W[j,1])^2) + sqrt((tw2[i]-W[j,2])^2) + sqrt((tw3[i]-W[j,3])^2) + sqrt((ta1[i]-A[j,1])^2) + sqrt((ta2[i]-A[j,2])^2) + sqrt((ta3[i]-A[j,3])^2)
        end
    end

    stepmat = zeros(size(d))
    # reset d to be used as c matrix instead of allocating a new array
    #d[1,:] = d[1,:]
    d[:,1] = cumsum(d[:,1])
    prevstart = 1:size(d,2)
    thisstart = zeros(size(d,2),1)
    println("calculating cost matrix")
    for i=2:size(d,1)
        thisstart = zeros(size(d,2),1)
        #fill!(thisstart,0)
        thisstart[1] = 1
        for j=2:size(d,2)
            m = minimum([d[i-1,j] d[i-1,j-1] d[i,j-1]])
            ind = argmin([d[i-1,j] d[i-1,j-1] d[i,j-1]])
            start1 = [prevstart[j] prevstart[j-1] thisstart[j-1]]
            d[i,j] = d[i,j] + m
            thisstart[j] = start1[ind]
            stepmat[i,j] = ind[2]
        end
        prevstart = thisstart
    end

    plot(imresize(d[end,:],2000),legend=false,show=true)
    plot!(size=(1600,400))
    println("Enter a threshold")
    tstring = readline()
    thresh = -1*(parse(Float64,tstring))
    println(thresh)
    # minx, properties = findpeaks1d(-c[end,:],width=tFF/walkinfo.PERIOD,prominence=thresh)    
    minx, properties = findpeaks1d(-d[end,:],width=tFF/walkinfo.PERIOD,height=thresh)  
    endpoint = minx

    xinds = zeros(Int,0)
    yinds = zeros(Int,0)
    append!(xinds,Int(size(d,1)))
    append!(yinds,endpoint[stridenum])
    i = Int(size(d,1))
    j = endpoint[stridenum]

    while i > 1
        step1 = stepmat[i,j]
        if step1 == 1
            append!(xinds,i-1)
            append!(yinds,j)
            i = i-1
        elseif step1 == 2
            append!(xinds,i-1)
            append!(yinds,j-1)
            i = i-1
            j = j-1
        elseif step1 == 3
            append!(xinds,i)
            append!(yinds,j-1)
            j = j - 1 
        else
            println(xinds[end])
            println(yinds[end])
            println(step1)
        end
    end

    return [xinds yinds]

end

function count_walking_bout(strides::StrideData, optVar=nothing)
    # returns an array with two columns
    # the first is the number of steps in each walking bout
    # the second is distance of each walking bout
    if optVar === nothing
        optVar = config()
    end
    goodinds = findall(x -> x < 10, strides.length) # only use steps shorter than 10m
    walkingBout = zeros(0)
    walkingBoutDist = zeros(0)
    nsteps = 1
    dist = strides.length[goodinds[1]]
    for i=2:length(goodinds)
        if (strides.startEnd[goodinds[i],1] - strides.startEnd[goodinds[i-1],2] <= optVar.maxWalkDif) 
            # this step starts less than a couple seconds from where the previous step ends
            nsteps=nsteps+1
            dist = dist+strides.length[goodinds[i]]
        else
            append!(walkingBout,nsteps)
            append!(walkingBoutDist,dist)
            nsteps = 1
            dist = strides.length[goodinds[i]]
        end
    end
    if nsteps > 1
        append!(walkingBout,nsteps)
        append!(walkingBoutDist,dist)
    end
    return [walkingBout walkingBoutDist]
end


function merge_strides(strideArr::Array{StrideData,1})
    # takes an array of StrideData 
    # and turns it into a single StrideData
    # with all strides combined together
    len = length(strideArr)
    totalstrides = 0
    steplength = 1
    for i=1:len
        totalstrides = totalstride+length(strideArr[i].length)
        if size(strideArr[i].frwd,2) > steplength
            steplength = size(strideArr[i].frwd,2)
        end
    end
    frwdSwing = zeros(totalstrides,steplength)
    ltrlSwing = zeros(totalstrides,steplength)
    frwd = zeros(totalstrides,steplength)
    ltrl = zeros(totalstrides,steplength)
    absLtrl = zeros(totalstrides,steplength)
    absFrwd = zeros(totalstrides,steplength)
    elev = zeros(totalstrides,steplength)
    theta = zeros(totalstrides,steplength)

    startEnd = zeros(totalstrides,2)
    footHeading = zeros(totalstrides)
    diffFootHeading = zeros(totalstrides)
    stepSamples = zeros(totalstrides)
    time = zeros(totalstrides)
    frwdSpeed = zeros(totalstrides)
    length1 = zeros(totalstrides)

    ind = 1
    for i=1:len
        frwdSwing[ind:ind+length(strideArr[i].length)-1,1:size(strideArr[i].frwd,2)] .= strideArr[i].frwdSwing
        ltrlSwing[ind:ind+length(strideArr[i].length)-1,1:size(strideArr[i].frwd,2)] .= strideArr[i].ltrlSwing
        frwd[ind:ind+length(strideArr[i].length)-1,1:size(strideArr[i].frwd,2)] .= strideArr[i].frwd
        ltrl[ind:ind+length(strideArr[i].length)-1,1:size(strideArr[i].frwd,2)] .= strideArr[i].ltrl
        absLtrl[ind:ind+length(strideArr[i].length)-1,1:size(strideArr[i].frwd,2)] .= strideArr[i].absLtrl
        absFrwd[ind:ind+length(strideArr[i].length)-1,1:size(strideArr[i].frwd,2)] .= strideArr[i].absFrwd
        elev[ind:ind+length(strideArr[i].length)-1,1:size(strideArr[i].frwd,2)] .= strideArr[i].elev
        theta[ind:ind+length(strideArr[i].length)-1,1:size(strideArr[i].frwd,2)] .= strideArr[i].theta
        startEnd[ind:ind+length(strideArr[i].length)-1,:] .= strideArr[i].startEnd
        # 1D arrays
        footHeading[ind:ind+length(strideArr[i].length)-1] .= strideArr[i].footHeading
        diffFootHeading[ind:ind+length(strideArr[i].length)-1] .= strideArr[i].diffFootHeading
        stepSamples[ind:ind+length(strideArr[i].length)-1] .= strideArr[i].stepSamples
        time[ind:ind+length(strideArr[i].length)-1] .= strideArr[i].time
        frwdSpeed[ind:ind+length(strideArr[i].length)-1] .= strideArr[i].frwdSpeed
        length1[ind:ind+length(strideArr[i].length)-1] .= strideArr[i].length

        # if there's space in the 2nd dimension of the array
        # fill it in with the last value for each stride
        if size(strideArr[i].frwd,2) < steplength
            for j=ind:ind+length(strideArr[i].length)-1
                frwdSwing[j,size(strideArr[i].frwd,2)+1:end] .= frwdSwing[j,size(strideArr[i].frwd,2)]
                ltrlSwing[j,size(strideArr[i].frwd,2)+1:end] .= ltrlSwing[j,size(strideArr[i].frwd,2)]
                frwd[j,size(strideArr[i].frwd,2)+1:end] .= frwd[j,size(strideArr[i].frwd,2)]
                ltrl[j,size(strideArr[i].frwd,2)+1:end] .= ltrl[j,size(strideArr[i].frwd,2)]
                absLtrl[j,size(strideArr[i].frwd,2)+1:end] .= absLtrl[j,size(strideArr[i].frwd,2)]
                absFrwd[j,size(strideArr[i].frwd,2)+1:end] .= absFrwd[j,size(strideArr[i].frwd,2)]
                elev[j,size(strideArr[i].frwd,2)+1:end] .= elev[j,size(strideArr[i].frwd,2)]
                theta[j,size(strideArr[i].frwd,2)+1:end] .= theta[j,size(strideArr[i].frwd,2)]
            end
        end
        ind = ind+length(strideArr[i].length)
    end

    newstrides =  StrideData(frwdSwing,ltrlSwing,frwd,ltrl,absLtrl,absFrwd,elev,theta,startEnd,footHeading,diffFootHeading,stepSamples,time,frwdSpeed,length1)
    return newstrides

end





#filename = "/Users/elizabeth/Documents/all-day-imus-code/multi-day/new/all-day-imus-julia/20210519-145905_step_detection_test.h5"
#filename = "/Volumes/Untitled/subject19/day1/20210317-134938_subject19_day1.h5"
#filename1 = "/Volumes/Untitled/subject3/day1/20200124-094306_subject3_day1_sensor_1326_label_left_foot.h5"
#filename2 = "/Volumes/Untitled/subject3/day1/20200124-094308_subject3_day1_sensor_1324_label_right_foot.h5"
#filename3 = "/Volumes/Untitled/subject3/day1/20200124-094306_subject3_day1_sensor_1334_label_waist.h5"
#filenames = [filename1; filename2; filename3]
#IMUarray = sync_apdm_separate(filenames)
#IMUarray = sync_apdm_combined(filename,["left","right","waist"])


default(legend=false) # for the plots
subject = 8
days = 1
path = "/Volumes/Untitled/" # base dir where data are stored
# this will look for .h5 files in subdirectories labelled like path/subject1/day1 
for day in days
pathFull = path * "subject" * string(subject) * "/day" * string(day) * "/"
h5s = glob("*.h5",pathFull)
optVar = config() # if you want to change any of the default parameters do it here
combined = false 
flabels = "abcdefg" # used if more than one h5 file per day
nfiles = 1


if length(h5s) == 1 # only one file in this directory
    filenames = h5s
    println("Found one file for this day: ")
    println(filenames[1])
    combined = true
else
# more than one file in this directory
# check if left, right, waist are separate files (most likely)
# or if day is split into multiple parts
    leftcount = 0
    rightcount = 0
    waistcount = 0
    leftFiles = String[]
    rightFiles = String[]
    waistFiles = String[]
    for i=1:length(h5s)
        if occursin("left",h5s[i])
            leftcount+=1
            push!(leftFiles,h5s[i])
        elseif occursin("right",h5s[i])
            rightcount+=1
            push!(rightFiles,h5s[i])
        elseif occursin("waist",h5s[i])
            waistcount+=1
            push!(waistFiles,h5s[i])
        end
    end
    if waistcount == 0 && leftcount == 0 && rightcount == 0
        filenames = h5s
        nfiles = length(h5s)
        combined = true
    elseif leftcount == rightcount
        nfiles = leftcount
    else
        println("Warning: unequal number of left and right files")
        nfiles = minimum([leftcount rightcount])
    end
end

for ifile = 1:nfiles
    if combined
        println("Using file ",filenames[ifile])
        IMUarray = sync_apdm_combined(filenames[ifile],["left","right","waist"])
    else
        # all of the days where the clock had to be set manually
        if subject == 3 && day == 4
            IMUarray = sync_apdm_bad_time([leftFiles[ifile] rightFiles[ifile] waistFiles[ifile]],1,-0.15)
        elseif subject == 3 && day == 5 && ifile == 1
            IMUarray = sync_apdm_bad_time([leftFiles[ifile] rightFiles[ifile] waistFiles[ifile]],1,29)
        elseif subject == 3 && day == 5 && ifile == 2
            IMUarray = sync_apdm_bad_time([leftFiles[ifile] rightFiles[ifile] waistFiles[ifile]],1,-114)
        elseif subject == 3 && day == 6 
            IMUarray = sync_apdm_bad_time([leftFiles[ifile] rightFiles[ifile] waistFiles[ifile]],1,-0.15)
        elseif subject == 3 && day == 7 && ifile == 1
            IMUarray = sync_apdm_bad_time([leftFiles[ifile] rightFiles[ifile] waistFiles[ifile]],1,5.8)
        elseif subject == 3 && day == 7 && ifile == 2   
            IMUarray = sync_apdm_bad_time([leftFiles[ifile] rightFiles[ifile] waistFiles[ifile]],1,8)
        elseif subject == 14 && day == 1
            IMUarray = sync_apdm_bad_time([leftFiles[ifile] rightFiles[ifile] waistFiles[ifile]],2,11190*0.0078)
        elseif subject == 14 && day == 2
            IMUarray = sync_apdm_bad_time([leftFiles[ifile] rightFiles[ifile]],2,-613*0.0078)
        elseif subject == 14 && day == 3
            IMUarray = sync_apdm_bad_time([leftFiles[ifile] rightFiles[ifile] waistFiles[ifile]],2,8053*0.0078)
        elseif subject == 14 && day == 4
            IMUarray = sync_apdm_bad_time([leftFiles[ifile] rightFiles[ifile] waistFiles[ifile]],2,16410*0.0078)
        elseif subject == 14 && day == 5
            IMUarray = sync_apdm_bad_time([leftFiles[ifile] rightFiles[ifile] waistFiles[ifile]],2,-605*0.0078)
        elseif subject == 14 && day == 7
            IMUarray = sync_apdm_bad_time([leftFiles[ifile] rightFiles[ifile] waistFiles[ifile]],2,22386*0.0078)

        # the days with working clocks
        elseif waistcount >= ifile # if waist is available
            println("Using files: ")
            println(leftFiles[ifile])
            println(rightFiles[ifile])
            println(waistFiles[ifile])
            IMUarray = sync_apdm_separate([leftFiles[ifile] rightFiles[ifile] waistFiles[ifile]])
        else # no waist; just use right and left foot
            println("Using files: ")
            println(leftFiles[ifile])
            println(rightFiles[ifile])
            IMUarray = sync_apdm_separate([leftFiles[ifile] rightFiles[ifile]])
        end
    end

    leftIMU = IMUarray[1]
    rightIMU = IMUarray[2]
    if length(IMUarray) > 2 
        waistIMU = IMUarray[3]
    else 
        waistIMU = nothing
    end
    walkSection = findwalking(leftIMU,rightIMU,waistIMU)
    walkSection = mergewalking(walkSection,optVar.maxWalkDif/leftIMU.PERIOD) # merge walking sections that are close together
    walkFilter = true
    leftWalkInfo = computepos(leftIMU, walkSection, optVar)
    rightWalkInfo = computepos(rightIMU, walkSection, optVar)
    leftStrides = getsteps(leftWalkInfo, optVar)
    rightStrides = getsteps(rightWalkInfo, optVar)
    t = (1:size(leftIMU.W)[1]) * leftIMU.PERIOD
    # Plot W and A
    p1 = plot(imresize(t,2000), imresize(leftIMU.W,(2000,3)), xlabel="Time (s)", ylabel="Angular vel")
    p2 = plot(imresize(t,2000), imresize(leftIMU.A,(2000,3)), xlabel="Time (s)", ylabel="Acceleration")
    plot(p1, p2, layout = (2,1), legend=false, show = true)
    sublabel = "s" * string(subject) * "d" * string(day)
    subfolder1 = "subject" * string(subject) * "/"
    subfolder2 = subfolder1* sublabel * "/"
    if !isdir(path*"results/") # create folder for results if it doesn't exist
        mkdir(path*"results/")
    end
    if !isdir(path*"results/"*subfolder1) # create subfolder for results if it doesn't exist
        mkdir(path*"results/"*subfolder1)
    end
    if !isdir(path*"results/"*subfolder2) 
        mkdir(path*"results/"*subfolder2)
    end
    if nfiles > 1
        sublabel = sublabel * flabels[ifile]
    end
    plotfile = path * "results/" * subfolder2 * sublabel * "-data-left.png"
    png(plotfile)
    # plot right foot W and A
    t = (1:size(rightIMU.W)[1]) * rightIMU.PERIOD
    p1 = plot(imresize(t,2000), imresize(rightIMU.W,(2000,3)), xlabel="Time (s)", ylabel="Angular vel")
    p2 = plot(imresize(t,2000), imresize(rightIMU.A,(2000,3)), xlabel="Time (s)", ylabel="Acceleration")
    plot(p1, p2, layout = (2,1), legend=false, show = true)
    plotfile = path * "results/" * subfolder2 * sublabel * "-data-right.png"
    png(plotfile)
    # Find walking sections with DTW
    # check if there's a file with saved thresholds
    threshfilel = path * "results/" * subfolder2 * sublabel * "threshl.csv"
    threshfiler = path * "results/" * subfolder2 * sublabel * "threshr.csv"
    if isfile(threshfilel) && isfile(threshfiler)
        result = DTWfindwalking(leftWalkInfo,leftStrides,rightWalkInfo,rightStrides, threshfilel=threshfilel,threshfiler=threshfiler,optVar=optVar)
        DTWleft = result[1]
        DTWright = result[2]
    else
        result = DTWfindwalking(leftWalkInfo,leftStrides,rightWalkInfo,rightStrides,optVar=optVar)
        DTWleft = result[1]
        DTWright = result[2]
        writedlm(threshfilel, [DTWleft.threshinds DTWleft.threshs], ',')
        writedlm(threshfiler, [DTWright.threshinds DTWright.threshs], ',')
    end

    walkSection2 = DTWleft.walkSection .| DTWright.walkSection
    DTWleft.walkSection = walkSection2
    DTWright.walkSection = walkSection2
    # re-calculate everything with new walking section
    leftWalkInfo = computepos(leftIMU, walkSection2, optVar)
    rightWalkInfo = computepos(rightIMU, walkSection2, optVar)
    leftStrides = getsteps(leftWalkInfo, optVar)
    rightStrides = getsteps(rightWalkInfo, optVar)
    leftStrides = findmissedsteps(leftIMU,leftStrides,leftWalkInfo,optVar)
    rightStrides = findmissedsteps(rightIMU,rightStrides,rightWalkInfo,optVar)
    # save to file
    savefile = path * "results/" * subfolder2 * sublabel * ".jld2"
    FileIO.save(savefile,"leftWalkInfo",leftWalkInfo,"rightWalkInfo",rightWalkInfo,"leftStrides",leftStrides,"rightStrides",rightStrides,"DTWleft",DTWleft,"DTWright",DTWright)
    println("Making plots")
    # stride plot
    leftinds = findall(x -> abs(x) < 10, leftStrides.length)
    if (length(leftStrides.length) - length(leftinds)) > 0
        println("Not displaying ",length(leftStrides.length) - length(leftinds)," strides longer than 10m (left foot)") 
    end
    scatter(leftStrides.ltrl[leftinds,end],leftStrides.frwd[leftinds,end],legend=false)
    for i in leftinds
        plot!(leftStrides.ltrl[i,:],leftStrides.frwd[i,:],legend=false)
    end
    plot!(leftStrides.ltrl[leftinds[1],:],leftStrides.frwd[leftinds[1],:],legend=false,show=true)
    xlabel!("ltrl (m)")
    ylabel!("frwd (m)")
    plotfile = path * "results/" * subfolder2 * sublabel * "-strides-left.pdf"
    savefig(plotfile)

    # walking section
    walkSectionn = zeros(Int,size(walkSection2))
    for i=1:length(walkSectionn)
    walkSectionn[i] = Int(walkSection2[i])
    end
    walkpath = path * "results/" * subfolder2 * sublabel * "-walk-section.csv"
    writedlm(walkpath,walkSectionn)
    walkImage = zeros(2000,200)
    walkImage[:,1] = imresize(walkSectionn,2000)
    for i=2:200
        walkImage[:,i] = walkImage[:,1]
    end
    heatmap(transpose(walkImage))
    plot!(size=(1600,200))
    plotfile = path * "results/" * subfolder2 * sublabel * "-walk-section.png"
    png(plotfile)

    # save a plot of the distance function
    plot(imresize(DTWleft.distanceFunc,2000),xticks = 0:100:2000,legend=false,show=true)
    plot!(size=(1600,400))
    plotfile = path * "results/" * subfolder2 * sublabel * "-distanceFunc-left.png"   
    png(plotfile)
    plot(imresize(DTWright.distanceFunc,2000),xticks = 0:100:2000,legend=false,show=true)
    plot!(size=(1600,400))
    plotfile = path * "results/" * subfolder2 * sublabel * "-distanceFunc-right.png"   
    png(plotfile)

    # stride length vs. speed
    xtest = leftStrides.frwdSpeed[leftinds]
    ytest = leftStrides.length[leftinds]
    layout = @layout [a            _
                    b{0.8w,0.8h} c]
    plot(layout = layout, link = :both, size = (500, 500), margin = -10Plots.px,legend=false)
    scatter!(xtest,ytest, subplot = 2, framestyle = :box, xlabel = "Forward stride velocity", ylabel = "Forward stride length")
    histogram!([xtest ytest], subplot = [1 3], orientation = [:v :h], framestyle = :none)
    plotfile = path * "results/" * subfolder2 * sublabel * "-length-speed-left.png"
    png(plotfile)

    # walking bout histogram
    result = count_walking_bout(leftStrides,optVar)
    walkingBout = result[:,1]
    walkingBoutDist = result[:,2]
    histogram(walkingBout)
    xlabel!("Steps in walking bout")
    txt = "Total steps: " * string(length(leftStrides.length))
    Plots.annotate!(minimum(walkingBout) + 0.9*(maximum(walkingBout)-minimum(walkingBout)), 6, text(txt, :black, :right))
    plotfile = path * "results/" * subfolder2 * sublabel * "-walking-bout-left.png"
    png(plotfile)
    # walking bouts under 100 steps
    smallbouts = findall(x -> x < 100, walkingBout)
    histogram(walkingBout[smallbouts])
    xlabel!("Steps in walking bout (under 100)")
    plotfile = path * "results/" * subfolder2 * sublabel * "-walking-bout-small-left.png"
    png(plotfile)
    # walking bout distance
    histogram(walkingBoutDist)
    xlabel!("Distance of walking bout (m)")
    plotfile = path * "results/" * subfolder2 * sublabel * "-walking-bout-dist-left.png"
    png(plotfile)
    # foot heading
    histogram(leftStrides.footHeading[leftinds])
    xlabel!("Foot heading")
    plotfile = path * "results/" * subfolder2 * sublabel * "-foot-heading-left.png"
    png(plotfile)

    # make all plots again with right foot
    # stride plot
    rightinds = findall(x -> abs(x) < 10, rightStrides.length)
    if (length(rightStrides.length) - length(rightinds)) > 0
        println("Not displaying ",length(rightStrides.length) - length(rightinds)," strides longer than 10m (right foot)") 
    end
    scatter(rightStrides.ltrl[rightinds,end],rightStrides.frwd[rightinds,end],legend=false)
    for i in rightinds
        plot!(rightStrides.ltrl[i,:],rightStrides.frwd[i,:],legend=false)
    end
    plot!(rightStrides.ltrl[rightinds[1],:],rightStrides.frwd[rightinds[1],:],legend=false,show=true)
    xlabel!("ltrl (m)")
    ylabel!("frwd (m)")
    plotfile = path * "results/" * subfolder2 * sublabel * "-strides-right.png"
    png(plotfile)
    # stride length vs. speed
    xtest = rightStrides.frwdSpeed[rightinds]
    ytest = rightStrides.length[rightinds]
    layout = @layout [a            _
                    b{0.8w,0.8h} c]
    plot(layout = layout, link = :both, size = (500, 500), margin = -10Plots.px,legend=false)
    scatter!(xtest,ytest, subplot = 2, framestyle = :box, xlabel = "Forward stride velocity", ylabel = "Forward stride length")
    histogram!([xtest ytest], subplot = [1 3], orientation = [:v :h], framestyle = :none)
    plotfile = path * "results/" * subfolder2 * sublabel * "-length-speed-right.png"
    png(plotfile)

    # walking bout histogram
    result = count_walking_bout(rightStrides,optVar)
    walkingBout = result[:,1]
    walkingBoutDist = result[:,2]
    histogram(walkingBout)
    xlabel!("Steps in walking bout")
    txt = "Total steps: " * string(length(rightStrides.length))
    Plots.annotate!(minimum(walkingBout) + 0.9*(maximum(walkingBout)-minimum(walkingBout)), 6, text(txt, :black, :right))
    plotfile = path * "results/" * subfolder2 * sublabel * "-walking-bout-right.png"
    png(plotfile)
    # walking bouts under 100 steps
    smallbouts = findall(x -> x < 100, walkingBout)
    histogram(walkingBout[smallbouts])
    xlabel!("Steps in walking bout (under 100)")
    plotfile = path * "results/" * subfolder2 * sublabel * "-walking-bout-small-right.png"
    png(plotfile)
    # walking bout distance
    histogram(walkingBoutDist)
    xlabel!("Distance of walking bout (m)")
    plotfile = path * "results/" * subfolder2 * sublabel * "-walking-bout-dist-right.png"
    png(plotfile)
    # foot heading
    histogram(rightStrides.footHeading[rightinds])
    xlabel!("Foot heading")
    plotfile = path * "results/" * subfolder2 * sublabel * "-foot-heading-right.png"
    png(plotfile)

end
end