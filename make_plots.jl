using Plots
using StatsBase
using Glob
using FileIO, JLD2
using LsqFit
using DelimitedFiles
using StatsFuns
using LinearAlgebra
using SpecialFunctions
using HypothesisTests
using DataFrames, GLM


mutable struct WalkData
    FFstart::Array{Int64,1}
    FFend::Array{Int64,1}
    walkSection::Array{Bool,1}
    Anz::Array{Float64,2}
    An::Array{Float64,2}
    A::Array{Float64,2}
    W::Array{Float64,2}
    V::Array{Float64,2}
    Vm::Array{Float64,1}
    P::Array{Float64,2}
    quaternion::Array{Float64,2}
    PERIOD::Float64
end

mutable struct StrideData
	frwdSwing::Array{Float64,2}
	ltrlSwing::Array{Float64,2}
	frwd::Array{Float64,2}  
	ltrl::Array{Float64,2}
	absLtrl::Array{Float64,2}
	absFrwd::Array{Float64,2}
	elev::Array{Float64,2}
    theta::Array{Float64,2}
	startEnd::Array{Float64,2}
	footHeading::Array{Float64,1}
	diffFootHeading::Array{Float64,1}
	stepSamples::Array{Float64,1} 
	time::Array{Float64,1} 
	frwdSpeed::Array{Float64,1} 
    length::Array{Float64,1}
end

function merge_strides(strideArr::Array{StrideData,1})
    # takes an array of StrideData 
    # and turns it into a single StrideData
    # with all strides combined together
    len = length(strideArr)
    totalstrides = 0
    steplength = 1
    for i=1:len
        totalstrides += length(strideArr[i].length)
        if size(strideArr[i].frwd,2) > steplength
            steplength = size(strideArr[i].frwd,2)
        end
    end
    frwdSwing = zeros(totalstrides,steplength)
    ltrlSwing = zeros(totalstrides,steplength)
    frwd = zeros(totalstrides,steplength)
    ltrl = zeros(totalstrides,steplength)
    absLtrl = zeros(totalstrides,steplength)
    absFrwd = zeros(totalstrides,steplength)
    elev = zeros(totalstrides,steplength)
    theta = zeros(totalstrides,steplength)

    startEnd = zeros(totalstrides,2)
    footHeading = zeros(totalstrides)
    diffFootHeading = zeros(totalstrides)
    stepSamples = zeros(totalstrides)
    time = zeros(totalstrides)
    frwdSpeed = zeros(totalstrides)
    length1 = zeros(totalstrides)

    ind = 1
    for i=1:len
        frwdSwing[ind:ind+length(strideArr[i].length)-1,1:size(strideArr[i].frwd,2)] .= strideArr[i].frwdSwing
        ltrlSwing[ind:ind+length(strideArr[i].length)-1,1:size(strideArr[i].frwd,2)] .= strideArr[i].ltrlSwing
        frwd[ind:ind+length(strideArr[i].length)-1,1:size(strideArr[i].frwd,2)] .= strideArr[i].frwd
        ltrl[ind:ind+length(strideArr[i].length)-1,1:size(strideArr[i].frwd,2)] .= strideArr[i].ltrl
        absLtrl[ind:ind+length(strideArr[i].length)-1,1:size(strideArr[i].frwd,2)] .= strideArr[i].absLtrl
        absFrwd[ind:ind+length(strideArr[i].length)-1,1:size(strideArr[i].frwd,2)] .= strideArr[i].absFrwd
        elev[ind:ind+length(strideArr[i].length)-1,1:size(strideArr[i].frwd,2)] .= strideArr[i].elev
        theta[ind:ind+length(strideArr[i].length)-1,1:size(strideArr[i].frwd,2)] .= strideArr[i].theta
        startEnd[ind:ind+length(strideArr[i].length)-1,:] .= strideArr[i].startEnd
        # 1D arrays
        footHeading[ind:ind+length(strideArr[i].length)-1] .= strideArr[i].footHeading
        diffFootHeading[ind:ind+length(strideArr[i].length)-1] .= strideArr[i].diffFootHeading
        stepSamples[ind:ind+length(strideArr[i].length)-1] .= strideArr[i].stepSamples
        time[ind:ind+length(strideArr[i].length)-1] .= strideArr[i].time
        frwdSpeed[ind:ind+length(strideArr[i].length)-1] .= strideArr[i].frwdSpeed
        length1[ind:ind+length(strideArr[i].length)-1] .= strideArr[i].length

        # if there's space in the 2nd dimension of the array
        # fill it in with the last value for each stride
        if size(strideArr[i].frwd,2) < steplength
            for j=ind:ind+length(strideArr[i].length)-1
                frwdSwing[j,size(strideArr[i].frwd,2)+1:end] .= frwdSwing[j,size(strideArr[i].frwd,2)]
                ltrlSwing[j,size(strideArr[i].frwd,2)+1:end] .= ltrlSwing[j,size(strideArr[i].frwd,2)]
                frwd[j,size(strideArr[i].frwd,2)+1:end] .= frwd[j,size(strideArr[i].frwd,2)]
                ltrl[j,size(strideArr[i].frwd,2)+1:end] .= ltrl[j,size(strideArr[i].frwd,2)]
                absLtrl[j,size(strideArr[i].frwd,2)+1:end] .= absLtrl[j,size(strideArr[i].frwd,2)]
                absFrwd[j,size(strideArr[i].frwd,2)+1:end] .= absFrwd[j,size(strideArr[i].frwd,2)]
                elev[j,size(strideArr[i].frwd,2)+1:end] .= elev[j,size(strideArr[i].frwd,2)]
                theta[j,size(strideArr[i].frwd,2)+1:end] .= theta[j,size(strideArr[i].frwd,2)]
            end
        end
        ind = ind+length(strideArr[i].length)
    end

    newstrides =  StrideData(frwdSwing,ltrlSwing,frwd,ltrl,absLtrl,absFrwd,elev,theta,startEnd,footHeading,diffFootHeading,stepSamples,time,frwdSpeed,length1)
    return newstrides

end


# include steps from both left and right together???
function count_walking_bout(strides::StrideData, maxdif)
    # returns an array with two columns
    # the first is the number of steps in each walking bout
    # the second is distance of each walking bout
    if maxdif === nothing
        maxdif = 512 # 4 seconds
    end
    goodinds = findall(x -> x < 10, strides.length) # only use steps shorter than 10m
    walkingBout = zeros(0)
    walkingBoutDist = zeros(0)
    nsteps = 1
    dist = strides.length[goodinds[1]]
    for i=2:length(goodinds)
        if (strides.startEnd[goodinds[i],1] - strides.startEnd[goodinds[i-1],2] <= maxdif) 
            # this step starts less than a couple seconds from where the previous step ends
            nsteps=nsteps+1
            dist = dist+strides.length[goodinds[i]]
        else
            append!(walkingBout,nsteps)
            append!(walkingBoutDist,dist)
            nsteps = 1
            dist = strides.length[goodinds[i]]
        end
    end
    append!(walkingBout,nsteps)
    append!(walkingBoutDist,dist)
    return [walkingBout walkingBoutDist]
end



function count_walking_bout_both(leftstrides::StrideData, rightstrides::StrideData, maxdif=512)
    # returns an array with two columns
    # the first is the number of steps in each walking bout
    # the second is distance of each walking bout
    if maxdif === nothing
        maxdif = 512 # 4 seconds
    end
    walkingBout = zeros(0)
    walkingBoutDist = zeros(0)
    nsteps = 1
    if leftstrides.startEnd[1,1] < rightstrides.startEnd[1,1]
        x = leftstrides.startEnd[1,2]
        nl = 2
        nr = 1
        dist = leftstrides.length[1]
    else
        x = rightstrides.startEnd[1,2]
        nl = 1
        nr = 2
        dist = rightstrides.length[1]
    end
    rightdone = false
    leftdone = false
    while !rightdone || !leftdone 
        if ((leftstrides.startEnd[nl,1] < rightstrides.startEnd[nr,1]) || rightdone) && !leftdone # next step is left foot
            if (leftstrides.startEnd[nl,1] - x <= maxdif) 
                nsteps = nsteps+1
                dist = dist+leftstrides.length[nl]
            else
                append!(walkingBout,nsteps)
                append!(walkingBoutDist,dist)
                nsteps = 1
                dist = leftstrides.length[nl]
            end
            x = leftstrides.startEnd[nl,2]
            nl = nl+1
            if nl > length(leftstrides.length)
                leftdone = true
                nl = length(leftstrides.length)
                #println("left is done")
            end
        else # (rightstrides.startEnd[nr,1] < leftstrides.startEnd[nl,1]) # next step is right foot
            if (rightstrides.startEnd[nr,1] - x <= maxdif)    
                nsteps = nsteps+1
                dist = dist+rightstrides.length[nr]
            else
                append!(walkingBout,nsteps)
                append!(walkingBoutDist,dist)
                nsteps = 1
                dist = rightstrides.length[nr]
            end
            x = rightstrides.startEnd[nr,2]
            nr = nr+1
            if nr > length(rightstrides.length)
                rightdone = true
                nr = length(rightstrides.length)
                #println("right is done")
            end
        end
    end
    append!(walkingBout,nsteps)
    append!(walkingBoutDist,dist)
    return [walkingBout walkingBoutDist]
end

function count_walking_bout_both2(leftstrides::StrideData, rightstrides::StrideData, maxdif=512)
    # returns an array with two columns
    # the first is the number of steps in each walking bout
    # the second is the average speed of each walking bout
    if maxdif === nothing
        maxdif = 512 # 4 seconds
    end
    walkingBout = zeros(0)
    avgSpeed = zeros(0)
    nsteps = 1
    leftcount = zeros(length(leftstrides.length))
    rightcount = zeros(length(rightstrides.length))
    thisleft = zeros(0) # left step indices in current walking bout
    thisright = zeros(0)
    speedtotal = 0
    if leftstrides.startEnd[1,1] < rightstrides.startEnd[1,1]
        x = leftstrides.startEnd[1,2]
        nl = 2
        nr = 1
        #speedtotal+=leftstrides.frwdSpeed[1]
        append!(thisleft,1)
    else
        x = rightstrides.startEnd[1,2]
        nl = 1
        nr = 2
        #speedtotal+=rightstrides.frwdSpeed[1]
        append!(thisright,1)
    end
    rightdone = false
    leftdone = false
    speedThisBout = zeros(0)
    while !rightdone || !leftdone 
        if ((leftstrides.startEnd[nl,1] < rightstrides.startEnd[nr,1]) || rightdone) && !leftdone # next step is left foot
            if (leftstrides.startEnd[nl,1] - x <= maxdif) 
                nsteps = nsteps+1
                append!(thisleft,nl)
            else
                append!(walkingBout,nsteps)
                speedtotal = 0
                if length(thisleft) > 0
                    leftcount[Int(thisleft[1]):Int(thisleft[end])] .= nsteps
                    for i=Int(thisleft[1]):Int(thisleft[end])
                        speedtotal+=leftstrides.frwdSpeed[i]
                    end
                end
                if length(thisright) > 0
                    rightcount[Int(thisright[1]):Int(thisright[end])] .= nsteps
                    for i=Int(thisright[1]):Int(thisright[end])
                        speedtotal+=rightstrides.frwdSpeed[i]
                    end
                end
                append!(avgSpeed,speedtotal/nsteps)
                nsteps = 1
                thisleft = zeros(0)
                thisright = zeros(0)
                append!(thisleft,nl)
            end
            x = leftstrides.startEnd[nl,2]
            nl = nl+1
            if nl > length(leftstrides.length)
                leftdone = true
                nl = length(leftstrides.length)
                #println("left is done")
            end
        else # (rightstrides.startEnd[nr,1] < leftstrides.startEnd[nl,1]) # next step is right foot
            if (rightstrides.startEnd[nr,1] - x <= maxdif)    
                nsteps = nsteps+1
                append!(thisright,nr)
            else
                append!(walkingBout,nsteps)
                speedtotal = 0
                speedThisBout = zeros(0)
                if length(thisleft) > 0
                    leftcount[Int(thisleft[1]):Int(thisleft[end])] .= nsteps
                    for i=Int(thisleft[1]):Int(thisleft[end])
                        speedtotal+=leftstrides.frwdSpeed[i]
                    end
                end
                if length(thisright) > 0
                    rightcount[Int(thisright[1]):Int(thisright[end])] .= nsteps
                    for i=Int(thisright[1]):Int(thisright[end])
                        speedtotal+=rightstrides.frwdSpeed[i]
                    end
                end
                append!(avgSpeed,speedtotal/nsteps)
                nsteps = 1
                thisleft = zeros(0)
                thisright = zeros(0)
                append!(thisright,nr)
            end
            x = rightstrides.startEnd[nr,2]
            nr = nr+1
            if nr > length(rightstrides.length)
                rightdone = true
                nr = length(rightstrides.length)
                #println("right is done")
            end
        end
    end
    append!(walkingBout,nsteps)
    speedtotal = 0
    if length(thisleft) > 0
        leftcount[Int(thisleft[1]):Int(thisleft[end])] .= nsteps
        for i=Int(thisleft[1]):Int(thisleft[end])
            speedtotal+=leftstrides.frwdSpeed[i]
        end
    end
    if length(thisright) > 0
        rightcount[Int(thisright[1]):Int(thisright[end])] .= nsteps
        for i=Int(thisright[1]):Int(thisright[end])
            speedtotal+=rightstrides.frwdSpeed[i]
        end
    end   
    append!(avgSpeed,speedtotal/nsteps)
    return [walkingBout avgSpeed]
end




function walking_bout_speeds(leftstrides::StrideData, rightstrides::StrideData, maxdif=512)
    # returns a vector of vectors containing the stride speeds for each walking bout
    # e.g. strideSpeeds[1] is a vector of stride speeds from all steps in the first walking bout
    if maxdif === nothing
        maxdif = 512 # 4 seconds
    end
    walkingBout = zeros(0)
    nsteps = 1
    strideSpeeds = Vector{Float64}[]
    speedThisBout = zeros(0)
    if leftstrides.startEnd[1,1] < rightstrides.startEnd[1,1]
        x = leftstrides.startEnd[1,2]
        nl = 2
        nr = 1
        append!(speedThisBout,leftstrides.frwdSpeed[1])
    else
        x = rightstrides.startEnd[1,2]
        nl = 1
        nr = 2
        append!(speedThisBout,rightstrides.frwdSpeed[1])
    end
    rightdone = false
    leftdone = false
    while !rightdone || !leftdone 
        if ((leftstrides.startEnd[nl,1] < rightstrides.startEnd[nr,1]) || rightdone) && !leftdone # next step is left foot
            if (leftstrides.startEnd[nl,1] - x <= maxdif) # add it to current walking bout
                nsteps = nsteps+1
                append!(speedThisBout,leftstrides.frwdSpeed[nl])
            else # start new walking bout
                append!(walkingBout,nsteps)
                push!(strideSpeeds,speedThisBout)
                nsteps = 1
                speedThisBout = zeros(0)
                append!(speedThisBout,leftstrides.frwdSpeed[nl])
            end
            x = leftstrides.startEnd[nl,2]
            nl = nl+1
            if nl > length(leftstrides.length)
                leftdone = true
                nl = length(leftstrides.length)
                #println("left is done")
            end
        else # (rightstrides.startEnd[nr,1] < leftstrides.startEnd[nl,1]) # next step is right foot
            if (rightstrides.startEnd[nr,1] - x <= maxdif)    
                nsteps = nsteps+1
                append!(speedThisBout,rightstrides.frwdSpeed[nr])
            else
                append!(walkingBout,nsteps)
                push!(strideSpeeds,speedThisBout)
                nsteps = 1
                speedThisBout = zeros(0)
                append!(speedThisBout,rightstrides.frwdSpeed[nr])
            end
            x = rightstrides.startEnd[nr,2]
            nr = nr+1
            if nr > length(rightstrides.length)
                rightdone = true
                nr = length(rightstrides.length)
                #println("right is done")
            end
        end
    end
    append!(walkingBout,nsteps)
    push!(strideSpeeds,speedThisBout)
    return strideSpeeds
end

function walking_bout_heading(leftstrides::StrideData, rightstrides::StrideData, maxdif=512)
    # like walking_bout_speeds but with relative heading instead of stride speed
    if maxdif === nothing
        maxdif = 512 # 4 seconds
    end
    walkingBout = zeros(0)
    nsteps = 1
    strideHeading = Vector{Float64}[]
    headThisBout = zeros(0)
    if leftstrides.startEnd[1,1] < rightstrides.startEnd[1,1]
        x = leftstrides.startEnd[1,2]
        nl = 2
        nr = 1
        append!(headThisBout,leftstrides.footHeading[1])
    else
        x = rightstrides.startEnd[1,2]
        nl = 1
        nr = 2
        append!(headThisBout,rightstrides.footHeading[1])
    end
    rightdone = false
    leftdone = false
    while !rightdone || !leftdone 
        if ((leftstrides.startEnd[nl,1] < rightstrides.startEnd[nr,1]) || rightdone) && !leftdone # next step is left foot
            if (leftstrides.startEnd[nl,1] - x <= maxdif) # add it to current walking bout
                nsteps = nsteps+1
                append!(headThisBout,leftstrides.footHeading[nl])
            else # start new walking bout
                append!(walkingBout,nsteps)
                push!(strideHeading,headThisBout)
                nsteps = 1
                headThisBout = zeros(0)
                append!(headThisBout,leftstrides.footHeading[nl])
            end
            x = leftstrides.startEnd[nl,2]
            nl = nl+1
            if nl > length(leftstrides.length)
                leftdone = true
                nl = length(leftstrides.length)
                #println("left is done")
            end
        else # (rightstrides.startEnd[nr,1] < leftstrides.startEnd[nl,1]) # next step is right foot
            if (rightstrides.startEnd[nr,1] - x <= maxdif)    
                nsteps = nsteps+1
                append!(headThisBout,rightstrides.footHeading[nr])
            else
                append!(walkingBout,nsteps)
                push!(strideHeading,headThisBout)
                nsteps = 1
                headThisBout = zeros(0)
                append!(headThisBout,rightstrides.footHeading[nr])
            end
            x = rightstrides.startEnd[nr,2]
            nr = nr+1
            if nr > length(rightstrides.length)
                rightdone = true
                nr = length(rightstrides.length)
                #println("right is done")
            end
        end
    end
    append!(walkingBout,nsteps)
    push!(strideHeading,headThisBout)
    return strideHeading
end



function findclearance(strides::StrideData)
    Gmin = 0.001 # the minimum clearance to determine if the foot is on the ground
    Poff = 0.4 # percent of ignored data just after the foot leaves the ground
    Pon = 0.3 # percent of ignored data just before foot returns to ground
    valid = ones(length(strides.length))
    clearance = zeros(length(strides.length))
    height = zeros(length(strides.length))
    for i = 1:length(strides.length)
        # strides.elev is negative 
        a0 = -strides.elev[i,end] / strides.frwd[i,end]
        distance = (-strides.elev[i,:] .- (strides.frwd[i,:].*a0))./sqrt(1+a0^2) # perpendicular distance between foot and virtual ground
        index_off = findfirst(x -> x > Gmin,distance) # index where the foot leaves the ground
        index_on = findlast(x -> x > Gmin,distance) # index where the foot returns to the ground
        height[i] = maximum(distance)
        if index_off === nothing
            valid[i] = 0
        else
            frwd_length=abs(strides.frwd[i,index_on]-strides.frwd[i,index_off])
            midpoint1 = findfirst(x -> x > frwd_length*Poff,abs.(strides.frwd[i,:]))
            midpoint2 = findfirst(x -> x > frwd_length*(1-Pon),abs.(strides.frwd[i,:]))
            if midpoint1===nothing || midpoint2===nothing
                valid[i] = 2 # strange trajactory
            else
                fpeak,fpeak_loc = findmax(distance[1:midpoint1])
                rpeak,rpeak_loc = findmax(distance[midpoint2+1:end])
                rpeak_loc=rpeak_loc+midpoint2
                smooth_section=fpeak_loc:rpeak_loc
                value,index = findmin(distance[smooth_section])
                index = index + fpeak_loc-1
                clearance[i] = value
                if value==fpeak
                    valid[i] = 3 # cannot find the front peak
                    # look for a section before this where 2nd derivative is negative
                    while true
                        first = diff(distance[1:index-1])
                        second = diff(first)
                        lastind = findlast(x -> x < 0, second) # the most recent point with negative 2nd derivative
                        if lastind === nothing
                            # no section of negative second derivative
                            break
                        else
                            firstind = findlast( x -> x > 0, second[1:lastind]) # the last point before that with positive 2nd derivative
                            if firstind === nothing
                                firstind = 0
                            end
                            # find the front peak in the negative 2nd derivative section
                            fpeak,fpeak_loc = findmax(distance[firstind+1:lastind+2])
                            fpeak_loc = fpeak_loc + firstind
                            smooth_section=fpeak_loc:rpeak_loc
                            value,index = findmin(distance[smooth_section])
                            index = index + fpeak_loc-1
                            clearance[i] = value
                            if value != fpeak #successfully found fpeak
                                valid[i] = 3.5
                                break
                            end
                        end
                    end
                elseif value==rpeak
                    valid[i] = 4 # cannot find the rear peak
                    # look for a section after this where 2nd derivative is negative
                    while true
                        first = diff(distance[index+1:end])
                        second = diff(first)
                        firstind = findfirst(x -> x < 0, second)
                        if firstind === nothing
                            # no section of negative second derivative
                            break
                        else
                            lastind = findfirst(x -> x > 0, second[firstind+1:end])
                            if lastind === nothing
                                lastind = length(second[firstind+1:end])+1
                            end
                            rpeak,rpeak_loc = findmax(distance[index+firstind:index+firstind+lastind+1])
                            rpeak_loc = rpeak_loc + index+firstind-1
                            smooth_section=fpeak_loc:rpeak_loc
                            value,index = findmin(distance[smooth_section])
                            index = index + fpeak_loc-1
                            clearance[i] = value
                            if value != rpeak
                                #succesfully found rpeak
                                valid[i] = 4.5
                                break
                            end
                        end
                    end
                end
            end
        end
    end

    return [clearance valid height]

end

function plotstridehorizontal(strides)
    inds = findall(x -> abs(x) < 10, strides.length)
    if (length(strides.length) - length(inds)) > 0
        println("Not displaying ",length(strides.length) - length(inds)," strides longer than 10m") 
    end
    scatter(strides.ltrl[inds,end],strides.frwd[inds,end],legend=false)
    for i in inds
        plot!(strides.ltrl[i,:],strides.frwd[i,:],legend=false)
    end
    plot!(strides.ltrl[inds[1],:],strides.frwd[inds[1],:],legend=false,show=true,size=(415,400),xlims=[-1.4 1.4],aspect_ratio=1)
    xlabel!("Lateral (m)")
    ylabel!("Forward (m)")
end

function plotstridevert(strides)
    inds = findall(x -> abs(x) < 10, strides.length)
    if (length(strides.length) - length(inds)) > 0
        println("Not displaying ",length(strides.length) - length(inds)," strides longer than 10m") 
    end
    scatter(strides.frwd[inds,end],-strides.elev[inds,end],legend=false)
    for i in inds
        plot!(strides.frwd[i,:],-strides.elev[i,:],legend=false)
    end
    plot!(strides.frwd[inds[1],:],-strides.elev[inds[1],:],legend=false,show=true,size=(415,400),xlims=[-0.7 2.1],ylims=[-1.4 1.4])
    xlabel!("frwd (m)")
    ylabel!("elev (m)")
end

function compute_cov(x,y)
    data = [x y]
    #data[:,1] = data[:,1] .- mean(data[:,1])
    #data[:,2] = data[:,2] .- mean(data[:,2])
    sigma = 1 # standard deviations
    # Computes the confidence value for a given sigma value 
    # 68.3%, sigma = 1; 95.4%, sigma = 2; 99.7%, sigma = 3 
    confidence_value = diff([normcdf(-sigma); normcdf(sigma)])
    # Compute the Mahalanobis radius of the ellipsoid that encloses
    # the desired probability confidence 
    scale = 2.2957  #chi2inv(confidence_value,2)
    # Scale the covariance matrix so the confidence region has unit
    # Mahalanobis distance.
    covar = cov(data) * scale
    # The axes of the covariance ellipse are given by the eigenvectors of
    # the covariance matrix. Their lengths (for the ellipse with unit
    # Mahalanobis radius) are given by the square roots of the
    # corresponding eigenvalues.
    V = eigvecs(covar)
    D = eigvals(covar)
    # Create ellipse data points
    t = range(0,stop=2*pi,length=100) 
    e = [cos.(t)  sin.(t)] # unit circle
    VV = [V[1,1]*sqrt(D[1]) V[1,2]*sqrt(D[2]); V[2,1]*sqrt(D[1]) V[2,2]*sqrt(D[2])] # scale eigenvectors
    e = VV*transpose(e)
    ex = e[1,:]
    ey = e[2,:]
    scatter(data[:,1],data[:,2],xlims = (-2,2))
    plot!(ex.+mean(data[:,1]),ey.+mean(data[:,2]),aspect_ratio = 1.)
    return e

end

function skewnormal(x1,mean1,std1,skew1)
    # skewed normal distribution
    # equation taken from wikipedia
    xshift = (x1 .- mean1)./std1
    y1 = 2/(sqrt(2*pi)) .* exp.(-(xshift.^2)./2) .* 1/2 .* (1 .+ erf.(skew1.*xshift./sqrt(2)))
    return y1 ./ sum(y1)
end



default(legend=false) # for the plots
subjects = [9 10 11 12 13 14 15 16 17 18 20 21 22 23] # older
#subjects = [1 2 3 4 5 6 7 8 19 24 25] # younger
days = 1:9
path = "/Volumes/Untitled/results/"
leglengths = [90.2 95 96 89 93.5 88 89 85 90.2 89 94 99 82.5 91.4 83.8 94 86.4 94 78.7 79 89 87 98 90 93]
leglengths = leglengths ./ 100 # convert to m
# find average normalization factors
n1 = zeros(length(subjects))
n2 = zeros(length(subjects))
for isub = 1:length(subjects)
    n1[isub] = leglengths[subjects[isub]]
    n2[isub] = sqrt(9.8*leglengths[subjects[isub]])
end
avglengthnorm = mean(n1)
avgspeednorm = mean(n2)
# height and weight not used for analysis, just for our records
heights = [175 184.5 176.5 170 177 170 163 157.6 160 165 175 174 152 170 165 178 163 183 160 158 157 172 183 177 167]
weights = [70.3 82.5 68 82 60 59 52 50.6 68.8 60.3 79.4 79.5 77.1 56.7 54.4 72.6 63.5 84.8 46 70.5 68 64 70 80 58]
hist_edges_speed = -1:0.01:2 # use for normalized numbers
hist_edges_length = -4:0.01:6
hist_edges_speed2 = -3:0.01:6 # use for non-normalized
hist_edges_length2 = -2:0.01:3 #-3:0.01:6
hist_edges_width = -2:0.01:2
hist_edges_time = 0:0.01:4
stepcedges = -0.1:0.01:0.2
fhedges = 0:0.01:pi
fhedges1 = -pi:0.01:pi
heightedges = 0:0.01:0.4
endedges = -0.6:0.01:0.6

frwd_all = zeros(0)
speed_all = zeros(0)
steps_all = zeros(0) # number of steps in each walking bout
time_all = zeros(0)
ltrl_all = zeros(0)
steps_all_norm = Vector{Float64}[]
clearance_hist = zeros(length(subjects),length(stepcedges)-1)
heading_hist = zeros(length(subjects),length(fhedges)-1)
heading_hist1 = zeros(length(subjects),length(fhedges1)-1)
height_hist = zeros(length(subjects),length(heightedges)-1)
end_hist = zeros(length(subjects),length(endedges)-1)
length_hist = zeros(length(subjects),length(hist_edges_length2)-1)
width_hist = zeros(length(subjects),length(hist_edges_width)-1)
speed_hist = zeros(length(subjects),length(hist_edges_speed2)-1)
clearance_all = zeros(0)
heading_all = zeros(0)
height_all = zeros(0)
end_all = zeros(0)
stats = zeros(length(subjects),20)
# mean and mode and std of speed, stride length, time, correlation between frwd and ltrl, mean clearance, std clearance, mean heading, std heading
fitlines = zeros(length(subjects),6)
# coefficients of length-speed fit
fitlineswb = zeros(length(subjects),6)
newfitlineswb = zeros(length(subjects),3)
# coefficients of walking bout peak speed vs. distance fit
#@. wbmodel(x,b) = b[1]*log(x*b[2]) # model used for fit of peak speed vs. distance
@. wbmodel(x,b) = b[1]*(1-exp(-x/b[2]))

steps_per_sub = zeros(length(subjects)) # how many steps each person takes
steps_per_subc = zeros(length(subjects)) # how many steps we find a virtual clearance for
none1 = 0
none2 = 0
none3 = 0
for isub = 1:length(subjects)
    subnum = subjects[isub]
    
    frwd_this_subject = zeros(0)
    speed_this_subject = zeros(0)
    time_this_subject = zeros(0)
    steps_this_subject = zeros(0)
    boutdist_this_subject = zeros(0) # distance of each walking bout
    peakspeed_this_subject = zeros(0) # peak speed of each walking bout
    avgspeed_this_subject = zeros(0) # mean speed of each walking bout
    clearance_this_subject = zeros(0)
    heading_this_subject = zeros(0)
    ltrl_this_subject = zeros(0)
    height_this_subject = zeros(0)
    end_this_subject = zeros(0)
    subfolder1 = "subject" * string(subnum) * "/"
    for iday=days
        # check if results file exists
        sublabel = "s" * string(subnum) * "d" * string(iday)
        subfolder2 = subfolder1 * sublabel * "/"
        pathFull = path * subfolder2 
        resultfile = "*.jld2"
        file = glob(resultfile,pathFull)
        # if no file, skip and move to next day/subject
        if length(file) > 0 
            strideArrl = Array{StrideData,1}(undef,length(file))
            strideArrr = Array{StrideData,1}(undef,length(file))
            for i=1:length(file)
                println("Results found: ",file[i])
                leftStrides = FileIO.load(file[i],"leftStrides")
                rightStrides = FileIO.load(file[i],"rightStrides")
                strideArrl[i] = leftStrides
                strideArrr[i] = rightStrides
            end
            leftStrides = merge_strides(strideArrl)
            rightStrides = merge_strides(strideArrr)

            #left_walking_bout = count_walking_bout(leftStrides,4/0.0078)
            #right_walking_bout = count_walking_bout(rightStrides,4/0.0078)
            #walking_bout = count_walking_bout_both(leftStrides,rightStrides,512)
            result2 = count_walking_bout_both(leftStrides,rightStrides)
            strideSpeeds = walking_bout_speeds(leftStrides,rightStrides)
            strideHeading = walking_bout_heading(leftStrides,rightStrides)
            maxHeading = 0.785
            walking_bout = result2[:,1]
            walking_bout_dist = result2[:,2]
            maxspeed = zeros(size(walking_bout_dist))
            headingInd = zeros(Int,0)
            for i=1:length(walking_bout_dist)
                maxspeed[i] = maximum(strideSpeeds[i])
                append!(avgspeed_this_subject,mean(strideSpeeds[i]))
                if maximum(strideHeading[i]) < maxHeading && minimum(strideHeading[i]) > -maxHeading
                    append!(headingInd,i)
                end
            end
            append!(steps_this_subject,walking_bout)
            append!(boutdist_this_subject,walking_bout_dist)
            append!(peakspeed_this_subject,maxspeed)


            leftinds = findall(x -> abs(x) < 10, leftStrides.length)
            if (length(leftStrides.length) - length(leftinds)) > 0
                println("Not displaying ",length(leftStrides.length) - length(leftinds)," strides longer than 10m (left foot)") 
                none1 += length(leftStrides.length) - length(leftinds)
            end
            #append!(steps_this_subject,left_walking_bout)
            append!(frwd_this_subject,leftStrides.length[leftinds])
            #append!(frwd_left_this_subject,leftStrides.length[leftinds])
            append!(speed_this_subject,leftStrides.frwdSpeed[leftinds])
            append!(time_this_subject,leftStrides.time[leftinds])
            append!(ltrl_this_subject,-leftStrides.ltrl[leftinds,end])
            append!(end_this_subject,-leftStrides.elev[leftinds,end])
            # now add the right steps 
            # since these are for summary plots, order doesn't matter
            rightinds = findall(x -> abs(x) < 10, rightStrides.length)
            if (length(rightStrides.length) - length(rightinds)) > 0
                println("Not displaying ",length(rightStrides.length) - length(rightinds)," strides longer than 10m (right foot)") 
                none1 += length(rightStrides.length) - length(rightinds)
            end
           # append!(steps_this_subject,right_walking_bout)
            append!(frwd_this_subject,rightStrides.length[rightinds])
            #append!(frwd_right_this_subject,rightStrides.length[rightinds])
            append!(speed_this_subject,rightStrides.frwdSpeed[rightinds])
            append!(time_this_subject,rightStrides.time[rightinds])
            append!(ltrl_this_subject,rightStrides.ltrl[rightinds,end])
            append!(end_this_subject,-rightStrides.elev[rightinds,end])


            # step clearance
            result = findclearance(leftStrides)
            clearance = result[:,1]
            valid = result[:,2]
            height = result[:,3]
            clearind = findall(x -> abs(x) < 1,clearance)
            validind = findall(x -> x == 1 || x == 3.5 || x == 4.5, valid)
            goodind = intersect(clearind,validind)
            if (length(clearance) - length(clearind)) > 0
                println("Not displaying ",length(clearance) - length(clearind)," strides with virtual clearance greater than 1m (left foot)") 
                none2 += length(clearance) - length(clearind)
            end
            if (length(clearance) - length(validind)) > 0
                println("Not displaying ",length(clearance) - length(validind)," strides where peaks couldn't be found for virtual clearance (left foot)") 
                none3 += length(clearance) - length(validind)
            end
            append!(clearance_this_subject,clearance[goodind])
            steps_per_subc[isub] += length(goodind)
            append!(height_this_subject,height[goodind])
            histogram(clearance[goodind])
            plotfile = pathFull * "clearance-left.png"
            #png(plotfile)
            result = findclearance(rightStrides)
            clearance = result[:,1]
            valid = result[:,2]
            height = result[:,3]
            clearind = findall(x -> abs(x) < 1,clearance)
            validind = findall(x -> x == 1 || x == 3.5 || x == 4.5, valid)
            goodind = intersect(clearind,validind)
            if (length(clearance) - length(clearind)) > 0
                println("Not displaying ",length(clearance) - length(clearind)," strides with virtual clearance greater than 1m (right) foot)") 
                none2 += length(clearance) - length(clearind)
            end
            if (length(clearance) - length(validind)) > 0
                println("Not displaying ",length(clearance) - length(validind)," strides with where peaks couldn't be found for virtual clearance (right) foot)") 
                none3 += length(clearance) - length(validind)
            end
            append!(clearance_this_subject,clearance[goodind])
            steps_per_subc[isub] += length(goodind)
            append!(height_this_subject,height[goodind])
            histogram(clearance[goodind])
            plotfile = pathFull * "clearance-right.png"
            #png(plotfile)

            # add foot heading
            fh = leftStrides.footHeading
            for i=1:length(fh)
                if fh[i] < -pi
                    fh[i] = fh[i] + 2*pi
                elseif fh[i] > pi
                    fh[i] = fh[i] - 2*pi
                end
            end
            append!(heading_this_subject,fh)
            fh = rightStrides.footHeading
            for i=1:length(fh)
                if fh[i] < -pi
                    fh[i] = fh[i] + 2*pi
                elseif fh[i] > pi
                    fh[i] = fh[i] - 2*pi
                end
            end
            append!(heading_this_subject,fh)

             # plot vertical trace of strides 
             #plotstridevert(leftStrides)
             #plotfile = pathFull * "stride-vert-left.png"
             #png(plotfile)
             #plotstridevert(rightStrides)
             #plotfile = pathFull * "stride-vert-right.png"
             #png(plotfile)

            #histogram(end_this_subject)
            #plotfile = pathFull * "stride-end-height.png"
            #png(plotfile)

        end
    end

    speed_this_subject_norm = speed_this_subject./sqrt(9.8*leglengths[subjects[isub]])
    frwd_this_subject_norm = frwd_this_subject./leglengths[subjects[isub]]
    ltrl_this_subject_norm = ltrl_this_subject./leglengths[subjects[isub]]
    append!(steps_all,steps_this_subject)
    #append!(boutspeed_all,boutspeed_this_subject)
    append!(frwd_all,frwd_this_subject_norm)
    append!(ltrl_all,ltrl_this_subject_norm)
    append!(speed_all,speed_this_subject_norm)
    append!(time_all,time_this_subject)
    append!(clearance_all,clearance_this_subject)
    append!(height_all,height_this_subject)
    append!(heading_all,heading_this_subject)
    append!(end_all,end_this_subject)
    steps_per_sub[isub] = sum(steps_this_subject)
    

    stats[isub,1] = mean(speed_this_subject_norm)
    h = fit(Histogram, speed_this_subject_norm, hist_edges_speed)
    m,ind = findmax(h.weights)
    stats[isub,2] = (hist_edges_speed[ind]+hist_edges_speed[ind+1])*0.5
    stats[isub,3] = std(speed_this_subject_norm)
    stats[isub,4] = mean(frwd_this_subject_norm)
    h = fit(Histogram, frwd_this_subject_norm, hist_edges_length)
    m,ind = findmax(h.weights)
    stats[isub,5] = (hist_edges_length[ind]+hist_edges_length[ind+1])*0.5
    stats[isub,6] = std(frwd_this_subject_norm)
    stats[isub,7] = mean(time_this_subject)
    h = fit(Histogram, time_this_subject, hist_edges_time)
    m,ind = findmax(h.weights)
    stats[isub,8] = (hist_edges_time[ind]+hist_edges_time[ind+1])*0.5
    stats[isub,9] = std(time_this_subject)
    stats[isub,10] = cov(frwd_this_subject,ltrl_this_subject)
    stats[isub,11] = cov(frwd_this_subject,frwd_this_subject)
    stats[isub,12] = cov(ltrl_this_subject,ltrl_this_subject)
    stats[isub,13] = mean(clearance_this_subject)
    stats[isub,14] = std(clearance_this_subject)
    stats[isub,15] = mean(heading_this_subject)
    stats[isub,16] = std(heading_this_subject)
    stats[isub,17] = mean(height_this_subject)
    stats[isub,18] = std(height_this_subject)
    stats[isub,19] = mean(end_this_subject)
    stats[isub,20] = std(end_this_subject)

    

    # stride length vs. speed
    layout = @layout [a            _
                  b{0.8w,0.8h} c]
    plot(layout = layout, link = :both, size = (500, 500), margin = -10Plots.px,legend=false)
    #scatter!(speed_this_subject,frwd_this_subject, subplot = 2, framestyle = :box, xlabel = "Forward stride velocity", ylabel = "Forward stride length")
    histogram2d!(speed_this_subject_norm,frwd_this_subject_norm, xlims = (-1,2), ylims = (-4,6), subplot = 2, framestyle = :box, xlabel = "Forward stride velocity", ylabel = "Forward stride length",c=:blues,legend=false)
    histogram!(speed_this_subject_norm, subplot = 1, orientation = :v, framestyle = :none,xlims = (-1,2))
    histogram!(frwd_this_subject_norm, subplot = 3, orientation = :h, framestyle = :none,ylims = (-4,6))
    # add a fit line
    @. model(x, b) = b[1]*(x^b[2])
    b0 = [1, 0.6]
    # use only positive speeds and lengths
    ind1 = findall(x -> x > 0, speed_this_subject_norm)
    ind2 = findall(x -> x > 0, frwd_this_subject_norm)
    ind3 = intersect(ind1,ind2)
    fit1 = curve_fit(model, speed_this_subject_norm[ind3], frwd_this_subject_norm[ind3], b0)
    x1 = 0:0.01:1.5
    y1 = model(x1,coef(fit1))
    ci = confidence_interval(fit1)
    #plot!(x1,y1,subplot=2,linecolor=:black)
    fitlines[isub,1] = coef(fit1)[1]
    fitlines[isub,2] = coef(fit1)[2]
    fitlines[isub,3] = ci[1][1]
    fitlines[isub,4] = ci[1][2]
    fitlines[isub,5] = ci[2][1]
    fitlines[isub,6] = ci[2][2]

    plotfile = path * subfolder1 * "length-speed-2.pdf"
    #savefig(plotfile)


    # plot peak speed vs. distance
    boutinds = findall(x -> abs(x) < 10000, boutdist_this_subject)
    if (length(boutdist_this_subject) - length(boutinds)) > 0
        println("Not displaying ",length(boutdist_this_subject) - length(boutinds)," walking bouts longer than 10,000m (peak speed vs. distance plot)") 
    end
    boutinds2 = findall(x -> abs(x) < 100, peakspeed_this_subject)
    if (length(peakspeed_this_subject) - length(boutinds2)) > 0
        println("Not displaying ",length(peakspeed_this_subject) - length(boutinds2)," walking bouts faster than 100 m/s (peak speed vs. distance plot)") 
    end
    binds = findall(x -> x > 0, boutdist_this_subject)
    if (length(boutdist_this_subject) - length(binds)) > 0
        println("Not displaying ",length(boutdist_this_subject) - length(binds)," walking bouts with negative distance (peak speed vs. distance plot)") 
    end
    binds2 = findall(x -> x > 0, peakspeed_this_subject)   
    goodinds = intersect(boutinds,boutinds2,binds,binds2)   
    scatter(boutdist_this_subject[goodinds],peakspeed_this_subject[goodinds],xaxis=:log,markeralpha = 0.4,markerstrokecolor = nothing, markershape = :circle)
    xlabel!("Walking bout distance (m)")
    ylabel!("Peak speed (m/s)")
    plotfile = path * subfolder1 * "peakspeed-dist.pdf"
    savefig(plotfile)
    #scatter(boutdist_this_subject[goodinds],avgspeed_this_subject[goodinds],xaxis=:log,markeralpha = 0.4,markerstrokealpha = 0.4)
    #xlabel!("Walking bout distance (m)")
    #ylabel!("Mean speed of bout (m/s)")
    #plotfile = path * subfolder1 * "avgspeed-dist.pdf"
    #savefig(plotfile)
    # make fit line
    b0 = [1.0 2.0]
    lower_p = [0.0; 0.001]
    fit1 = curve_fit(wbmodel,boutdist_this_subject[goodinds],peakspeed_this_subject[goodinds],vec(b0),lower = lower_p)
    ci = confidence_interval(fit1)
    fitlineswb[isub,1] = coef(fit1)[1]
    fitlineswb[isub,2] = coef(fit1)[2]
    fitlineswb[isub,3] = ci[1][1]
    fitlineswb[isub,4] = ci[1][2]
    fitlineswb[isub,5] = ci[2][1]
    fitlineswb[isub,6] = ci[2][2]
    x1 = [0.01:0.01:0.99; 1:1000]
    y1 = wbmodel(x1,fitlineswb[isub,1:2])
    plot!(x1,y1)    
    plotfile = path * subfolder1 * "peakspeed-dist-line.pdf"
    savefig(plotfile)
    # new fitting method
    df = DataFrame(A=log.(boutdist_this_subject[goodinds]), B=peakspeed_this_subject[goodinds])
    ols = lm(@formula(B  ~ A), df)
    coefs = coef(ols)
    rsquared = r2(ols)
    newfitlineswb[isub,1] = coefs[2]
    newfitlineswb[isub,2] = coefs[1]
    newfitlineswb[isub,3] = rsquared

    # stride length histogram
    frwd1 = frwd_this_subject_norm * avglengthnorm # use * avglengthnorm to switch from normalized units to dimensional units
    histogram(frwd1)
    xlabel!("Stride forward length")
    ylabel!("Frequency")
    h = fit(Histogram, frwd1, hist_edges_length2)
    length_hist[isub,:] .= h.weights
    plotfile = path * subfolder1 * "frwd-hist.png"
    #png(plotfile)

    # stride width
    ltrl1 = ltrl_this_subject_norm * avglengthnorm
    histogram(ltrl1)
    xlabel!("Stride width")
    ylabel!("Frequency")
    h = fit(Histogram, ltrl1, hist_edges_width)
    width_hist[isub,:] .= h.weights
    plotfile = path * subfolder1 * "ltrl-hist.png"
    png(plotfile)

    # stride speed histogram
    speed1 = speed_this_subject_norm * avgspeednorm
    histogram(speed1)
    xlabel!("Stride forward velocity")
    ylabel!("Frequency")
    h = fit(Histogram, speed1, hist_edges_speed2)
    speed_hist[isub,:] .= h.weights
    plotfile = path * subfolder1 * "speed-hist.png"
    #png(plotfile)

    # walking bout histogram
    histogram(steps_this_subject)
    xlabel!("Steps in each walking bout")
    ylabel!("Frequency")
    h = fit(Histogram, steps_this_subject, nbins=maximum(steps_this_subject))
    push!(steps_all_norm,h.weights) #/sum(h.weights))
    plotfile = path * subfolder1 * "walking-bouts.png"
    #png(plotfile)

    # step clearance histogram
    histogram(clearance_this_subject)
    xlabel!("Virtual step clearance")
    ylabel!("Frequency")
    h = fit(Histogram, clearance_this_subject, stepcedges)
    clearance_hist[isub,:] .= h.weights
    plotfile = path * subfolder1 * "step-clearance.png"
    #png(plotfile)

    # foot heading histogram
    histogram(heading_this_subject)
    xlabel!("Foot heading")
    ylabel!("Frequency")
    h = fit(Histogram, abs.(heading_this_subject), fhedges)
    heading_hist[isub,:] .= h.weights
    h = fit(Histogram, heading_this_subject, fhedges1)
    heading_hist1[isub,:] .= h.weights
    plotfile = path * subfolder1 * "foot-heading.png"
    #png(plotfile)

    # stride height histogram
    histogram(height_this_subject)
    xlabel!("Stride height")
    ylabel!("Frequency")
    h = fit(Histogram, height_this_subject, heightedges)
    height_hist[isub,:] .= h.weights
    plotfile = path * subfolder1 * "stride-height.png"
    #png(plotfile)

    # stride end height histogram
    histogram(end_this_subject)
    xlabel!("Stride end height")
    ylabel!("Frequency")
    h = fit(Histogram, end_this_subject, endedges)
    end_hist[isub,:] .= h.weights
    plotfile = path * subfolder1 * "stride-end-height.png"
    #png(plotfile)


end


# stride length vs. speed for all subjects
layout = @layout [a            _
              b{0.8w,0.8h} c]
plot(layout = layout, link = :both, size = (500, 500), margin = -10Plots.px,legend=false)
histogram2d!(speed_all,frwd_all, subplot = 2, framestyle = :box, xlims = (-1,2), ylims = (-4,6), xlabel = "Forward stride velocity", ylabel = "Forward stride length",c=:dense)
histogram!(frwd_all,subplot=3,orientation=:h,framestyle=:none,ylims = (-4,6))
histogram!(speed_all,subplot=1,orientation=:v,framestyle=:none,xlims = (-1,2))
#histogram!([speed_all frwd_all], subplot = [1 3], orientation = [:v :h], framestyle = :none)
# add a fit line
@. model(x, b) = b[1]*(x^b[2])
b0 = [1, 0.6]
# use only positive speeds and lengths
ind1 = findall(x -> x > 0, speed_all)
ind2 = findall(x -> x > 0, frwd_all)
ind3 = intersect(ind1,ind2)
fit1 = curve_fit(model, speed_all[ind3], frwd_all[ind3], b0)
x1 = 0:0.01:2
y1 = model(x1,coef(fit1))
#plot!(x1,y1,subplot=2,linecolor=:black)

plotfile = path * "all-length-speed.pdf"
#savefig(plotfile)

y1 = model(x1,fitlines[1,:])
plot(x1,y1)
for i=2:length(subjects)
    y1 = model(x1,fitlines[i,:])
    plot!(x1,y1, xlims = (-1,2), ylims = (-4,6), xlabel = "Forward stride velocity", ylabel = "Forward stride length")
end
plotfile = path * "all-length-speed-lines.pdf"
#savefig(plotfile)
#colors = get_color_palette(:auto,length(subjects))

# walking bout peak speed vs. distance
 x1 = 1:1000
 #y1 = wbmodel(x1,fitlineswb[1,:])
 y1 = newfitlineswb[1,1].*log.(x1).+newfitlineswb[1,2]
 plot(x1,y1,xaxis=:log)
 for i=2:length(subjects)
     #y1 = wbmodel(x1,fitlineswb[i,:])
     y1 = newfitlineswb[i,1].*log.(x1).+newfitlineswb[i,2]
     plot!(x1,y1, xlabel = "Walking bout distance (m)", ylabel = "Peak speed (m/s)",xaxis=:log)
 end
 plotfile = path * "walking-bout-fit.pdf"
 savefig(plotfile)
 writedlm(path*"new-walkbout-fitcoef.csv",newfitlineswb)



# walking bout histogram for everyone
# first find how big it needs to be
maxSteps = 0
for i = 1:length(steps_all_norm)
    if length(steps_all_norm[i]) > maxSteps
        maxSteps = length(steps_all_norm[i])
    end
end

step_hist_all = zeros(length(steps_all_norm),maxSteps)
for  i = 1:length(steps_all_norm)
    step_hist_all[i,1:length(steps_all_norm[i])] = steps_all_norm[i]
end
step_edges = 1:maxSteps
# step_hist_all is not normalized

# percent of steps in walking bout
testall = zeros(size(step_hist_all))
step_edges2 = 2:step_edges[end]+1
for i=1:size(step_hist_all,1)
    testall[i,:] = step_hist_all[i,:].*step_edges./steps_per_sub[i]
end
plot(step_edges2,cumsum(testall[1,:]))
for i=2:size(step_hist_all,1)
    plot!(step_edges2,cumsum(testall[i,:]))
    #subnum = subjects[i]
    #subfolder1 = "subject" * string(subnum) * "/"
    #plotfile = path * subfolder1 * "walking-bout-percent.png"
    #png(plotfile)
end
xlabel!("Length of walking bout (steps)")
plotfile = path  * "walking-bout-all-percent.pdf"
#savefig(plotfile)
# use this to find what size walking bouts make up X percent of steps
stepsum = zeros(size(testall))
lim75 = zeros(length(subjects)) 
for i=1:length(subjects)
    stepsum[i,:] = cumsum(testall[i,:]) # stepsum = what gets plotted
    ind1 = findfirst(x -> x >= 0.75,stepsum[i,:])
    lim75[i] = step_edges2[ind1]
end
# lim75 has the step counts for each subject where 75% of steps come from bouts that length or shorter 




# percent of steps less than x step clearance
plot(stepcedges[2:end],cumsum(clearance_hist[1,:])./sum(clearance_hist[1,:]))
for i=2:size(clearance_hist,1)
    plot!(stepcedges[2:end],cumsum(clearance_hist[i,:])./sum(clearance_hist[i,:]))
end
xlabel!("Step clearance")
plotfile = path  * "clearance-all-percent.pdf"
#savefig(plotfile)

# percent of steps less than x angle from forward
plot(fhedges[2:end],cumsum(heading_hist[1,:])./steps_per_sub[1])
for i=2:size(heading_hist,1)
    plot!(fhedges[2:end],cumsum(heading_hist[i,:])./steps_per_sub[i])
end
xlabel!("Angle from forward direction (absolute value)")
plotfile = path  * "heading-all-percent.pdf"
#savefig(plotfile)

# percent of steps less than x height
plot(heightedges[2:end],cumsum(height_hist[1,:])./steps_per_subc[1])
for i=2:size(height_hist,1)
    plot!(heightedges[2:end],cumsum(height_hist[i,:])./steps_per_subc[i])
end
xlabel!("Stride height (m)")
plotfile = path  * "height-all-percent.pdf"
#savefig(plotfile)

# percent of steps less than x length
#plot(hist_edges_length2[2:end],cumsum(length_hist[1,:])./sum(length_hist[1,:]))
#for i=2:size(length_hist,1)
#    plot!(hist_edges_length2[2:end],cumsum(length_hist[i,:])./sum(length_hist[i,:]))
#end
#xlabel!("Stride forward length (m)")
#plotfile = path  * "length-all-percent.pdf"
#savefig(plotfile)

# percent of steps less than x speed
#plot(hist_edges_speed2[2:end],cumsum(speed_hist[1,:])./sum(speed_hist[1,:]))
#for i=2:size(speed_hist,1)
#    plot!(hist_edges_speed2[2:end],cumsum(speed_hist[i,:])./sum(speed_hist[i,:]))
#end
#xlabel!("Stride forward speed (m/s)")
#plotfile = path  * "speed-all-percent.pdf"
#savefig(plotfile)

# normalize step_hist_all by number of walking bouts per subject
num_bouts_per_sub = zeros(length(steps_all_norm))
for  i = 1:length(steps_all_norm)
    step_hist_all[i,1:length(steps_all_norm[i])] = steps_all_norm[i]./sum(steps_all_norm[i])
    num_bouts_per_sub[i] = sum(steps_all_norm[i])
end
step_edges = 1:maxSteps
stepbins = vec(mean(step_hist_all,dims=1))
b1 = bar(step_edges[1:500], stepbins[1:500],ylims = [0 0.2], xlabel = "Steps in each walking bout", ylabel = "Frequency")
#avgBoutsPerDay = 228 # younger
avgBoutsPerDay = 248 # older
# add right axis with average number of bouts per day
bar!(twinx(b1),step_edges[1:500],stepbins[1:500] .* avgBoutsPerDay, ylims = [0 0.2*avgBoutsPerDay],ylabel = "Average number of bouts per day")
plotfile = path * "walking-bout-all.pdf"
#savefig(plotfile)

plot(step_edges,cumsum(step_hist_all[1,:]),xlim=(0, 500))
for i=2:size(step_hist_all,1)
    plot!(step_edges,cumsum(step_hist_all[i,:]),xlim=(0, 500))
end

# normalize clearance_hist by number of steps per subject
for i = 1:size(clearance_hist,1)
    clearance_hist[i,:] = clearance_hist[i,:] ./ steps_per_subc[i]
end

# plot virtual clearance
bar(stepcedges.+(0.5*(stepcedges[2]-stepcedges[1])),vec(mean(clearance_hist,dims=1)))
xlabel!("Stride virtual clearance")
ylabel!("Frequency")
plotfile = path * "clearance-all.pdf"
#savefig(plotfile)

# foot heading
for i = 1:size(heading_hist1,1) 
    heading_hist1[i,:] = heading_hist1[i,:] ./ steps_per_sub[i]
end
bar(180/pi*(fhedges1.+(0.5*(fhedges1[2]-fhedges1[1]))),vec(mean(heading_hist1,dims=1)))
xlabel!("Foot heading")
ylabel!("Frequency")
plotfile = path * "heading-all.pdf"
#savefig(plotfile)

# stride height
for i = 1:size(height_hist,1) 
    height_hist[i,:] = height_hist[i,:] ./ steps_per_subc[i]
end
bar(heightedges.+(0.5*(heightedges[2]-heightedges[1])),vec(mean(height_hist,dims=1)))
xlabel!("Stride height")
ylabel!("Frequency")
# add skewed normal distribution
heightx = heightedges[1:end-1].+(0.5*(heightedges[2]-heightedges[1]))
heighty = vec(mean(height_hist,dims=1))
m,ind = findmax(heighty)
mean1 = heightx[ind]
std1 = std(heightx,AnalyticWeights(heighty))
skew1 = skewness(heightx,AnalyticWeights(heighty))
y1 = skewnormal(heightx,mean1,std1,skew1)
# find median of result and shift to line up with histogram
m,ind = findmax(y1)
shifty1 = heightx[ind] - mean1
plot!(heightx.-shifty1,y1)
plotfile = path * "height-all.pdf"
#savefig(plotfile)


# stride end height
for i = 1:size(end_hist,1) 
    end_hist[i,:] = end_hist[i,:] ./ steps_per_sub[i]
end
bar(endedges.+(0.5*(endedges[2]-endedges[1])),vec(mean(end_hist,dims=1)))
xlabel!("Stride end height")
ylabel!("Frequency")
# add skewed normal distribution
heightx = endedges[1:end-1].+(0.5*(endedges[2]-endedges[1]))
heighty = vec(mean(end_hist,dims=1))
m,ind = findmax(heighty)
mean1 = heightx[ind]
std1 = std(heightx,AnalyticWeights(heighty))
skew1 = skewness(heightx,AnalyticWeights(heighty))
y1 = skewnormal(heightx,mean1,std1,skew1)
# find median of result and shift to line up with histogram
m,ind = findmax(y1)
shifty1 = heightx[ind] - mean1
plot!(heightx.-shifty1,y1)
plotfile = path * "end-height-all.pdf"
savefig(plotfile)

# stride width
for i = 1:size(width_hist,1) 
    width_hist[i,:] = width_hist[i,:] ./ steps_per_sub[i]
end
bar(hist_edges_width.+(0.5*(hist_edges_width[2]-hist_edges_width[1])),vec(mean(width_hist,dims=1)))
xlabel!("Stride width")
ylabel!("Frequency")
plotfile = path * "width-all-hist.pdf"
savefig(plotfile)

# stride length
# for i = 1:size(length_hist,1) 
#     length_hist[i,:] = length_hist[i,:] ./ steps_per_sub[i]
# end
# bar(hist_edges_length.+(0.5*(hist_edges_length[2]-hist_edges_length[1])),vec(mean(length_hist,dims=1)))
# xlabel!("Stride forward length")
# ylabel!("Frequency")
# # add skewed normal distribution
# heightx = hist_edges_length[1:end-1].+(0.5*(hist_edges_length[2]-hist_edges_length[1]))
# heighty = vec(mean(length_hist,dims=1))
# m,ind = findmax(heighty)
# mean1 = heightx[ind]
# std1 = std(heightx,AnalyticWeights(heighty))
# skew1 = skewness(heightx,AnalyticWeights(heighty))
# y1 = skewnormal(heightx,mean1,std1,skew1)
# # find median of result and shift to line up with histogram
# m,ind = findmax(y1)
# shifty1 = heightx[ind] - mean1
# plot!(heightx.-shifty1,y1)
#plotfile = path * "length-all-hist.pdf"
#savefig(plotfile)
# older group: mode = 1.435 (1.289 m) mean = 1.096 (0.984 m) std = 0.525 skewness = -0.732 kurtosis = 0.268
# 67.7 % of steps below mode
# younger group: mode = 1.565 (1.405 m) mean = 1.245 (1.117 m) std = 0.558 skewness = -0.820 kurtosis = 2.361
# 68.8% of steps below mode

# stride speed
#for i = 1:size(speed_hist,1) 
#     speed_hist[i,:] = speed_hist[i,:] ./ steps_per_sub[i]
#end
#bar(hist_edges_speed.+(0.5*(hist_edges_speed[2]-hist_edges_speed[1])),vec(mean(speed_hist,dims=1)))
#xlabel!("Stride forward velocity")
#ylabel!("Frequency")
# # add skewed normal distribution
# heightx = hist_edges_speed[1:end-1].+(0.5*(hist_edges_speed[2]-hist_edges_speed[1]))
# heighty = vec(mean(speed_hist,dims=1))
# m,ind = findmax(heighty)
# mean1 = heightx[ind]
# std1 = std(heightx,AnalyticWeights(heighty))
# skew1 = skewness(heightx,AnalyticWeights(heighty))
# y1 = skewnormal(heightx,mean1,std1,skew1)
# # find median of result and shift to line up with histogram
# m,ind = findmax(y1)
# shifty1 = heightx[ind] - mean1
# plot!(heightx.-shifty1,y1)
# plotfile = path * "speed-all-hist.pdf"
# #savefig(plotfile)
# older group: mode = 0.395 (1.171 m/s) mean = 0.291 (0.862 m/s) std = 0.154 skewness = -0.324 kurtosis = -0.391
# 68.5 % of steps below mode
# younger group: mode = 0.415 (1.230 m/s) mean = 0.350 (1.039 m/s) std = 0.186 skewness = 0.278 kurtosis = 2.029
# 61.1% of steps below mode

b2 = vec(mean(step_hist_all,dims=1))
bar(1:500, b2[1:500])
xlabel!("Steps in each walking bout")
ylabel!("Frequency")
plotfile = path * "walking-bout-all-small.pdf"
#savefig(plotfile)

# covariance plot with histograms of length and width
# change subject number depending on younger or older group 
subject = 9
day = 1
loadpath = "/Volumes/Untitled/"
sublabel = "s" * string(subject) * "d" * string(day)
subfolder = "subject" * string(subject) * "/" * sublabel * "/"
resultfile = loadpath * "results/" * subfolder * sublabel * ".jld2"
leftStrides = FileIO.load(resultfile,"leftStrides")
rightStrides = FileIO.load(resultfile,"rightStrides")
x = leftStrides.ltrl[:,end]
y = leftStrides.frwd[:,end]
e = compute_cov(x,y)

layout = @layout [a            _
              b{0.8w,0.8h} c]
plot(layout = layout, link = :both, size = (500, 500), margin = -10Plots.px,legend=false)
scatter!(x,y,subplot = 2, framestyle = :box, xlims = (-2,2), ylims = (-2,3),xlabel = "Lateral (m)",ylabel = "Forward (m)")
plot!(e[1,:].+mean(x),e[2,:].+mean(y),subplot = 2,aspect_ratio = 1.)
#histogram!(frwd_all,subplot=3,orientation=:h,framestyle=:none,ylims = (-2,3))
bar!(hist_edges_length2.+(0.5*(hist_edges_length2[2]-hist_edges_length2[1])),vec(mean(length_hist,dims=1)),subplot=3,orientation=:h,framestyle=:none)
#histogram!(ltrl_all,subplot=1,orientation=:v,framestyle=:none,xlims = (-2,2))
bar!(hist_edges_width.+(0.5*(hist_edges_width[2]-hist_edges_width[1])),vec(mean(width_hist,dims=1)),subplot=1,orientation=:v,framestyle=:none)
plotfile = path * "cov-hist-plot-norm.pdf"
savefig(plotfile)


#writedlm(path*"fitcoef.csv",fitlines)
println(mean(speed_all))
println(std(speed_all))
println(mean(frwd_all))
println(std(frwd_all))
println(mean(time_all))
println(std(time_all))


stats[:,1:3] = stats[:,1:3] .* avgspeednorm
stats[:,4:6] = stats[:,1:3] .* avglengthnorm
#writedlm(path*"stats.csv",stats)
 
# compare to stats from younger group
statsy = readdlm(path*"younger/stats.csv")

pvalues = zeros(size(stats,2))
for i=1:size(stats,2)
    ht = UnequalVarianceTTest(statsy[:,i],stats[:,i])
    pvalues[i] = pvalue(ht)
end